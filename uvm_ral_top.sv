`timescale 1ns/10ps

import uvm_pkg::*;

`include "my_pkg.sv"
import my_pkg::*;

`include "my_interface.sv"
`include "./RTL/my_regs.sv"
`include "./RTL/my_ram.sv"
`include "./RTL/my_rom.sv"

module uvm_ral_top;

  my_interface interface_inst();

  // Specify time unit and precision for simulation:
  //
  // Use nanoseconds (ns) as a time unit
  // Precision of two fractional digits
  // Append " ns" to every display of time value
  // Minimum field width of one symbol
  initial begin
    $timeformat(-9, 2, " ns", 1);
  end

  // Clock
  always begin
    #5 interface_inst.clk = ~interface_inst.clk;
  end

  // Set clk bit to '1' at the start of simulation
  initial begin
    interface_inst.clk = 1'b1;
  end

  // Instantiate registers
  my_regs regs_inst (
    .clk(interface_inst.clk),
    .reset(interface_inst.reset),
    .reg_addr(interface_inst.reg_addr),
    .write_enable(interface_inst.reg_write_enable),
    .reg_in(interface_inst.reg_in),
    .reg_out(interface_inst.reg_out)
    );
  
  // Instantiate RAM memory
  my_ram ram_inst (
    .clk(interface_inst.clk),
    .reset(interface_inst.reset),
    .ram_addr(interface_inst.ram_addr),
    .write_enable(interface_inst.ram_write_enable),
    .ram_in(interface_inst.ram_in),
    .ram_out(interface_inst.ram_out)
    );
  
  // Instantiate ROM memory
  my_rom rom_inst (
    .clk(interface_inst.clk),
    .reset(interface_inst.reset),
    .rom_addr(interface_inst.rom_addr),
    .rom_out(interface_inst.rom_out)
  );

  initial begin
    uvm_config_db#(virtual my_interface)::set(uvm_root::get(), "*", "interface_inst", interface_inst);

    // Type test name you want to execute
    run_test("my_full_test");
  end

endmodule