# Automatic Verification Methods in the SystemVerilog Register Access Layer

The goal of this project is to showcase how the UVM register abstraction layer (RAL) can be used to verify functionality of registers and memories.

## Testbench Structure

![Testbench Structure](Testbench Structure Diagram/final_testbench.drawio.png)

Individual components of this testbench are described in detail below.

### DUT Blocks

These blocks are used as an example to show how UVM RAL can be used to verify them.

1. [Block of Registers](./RTL/my_regs.sv)
2. [RAM](./RTL/my_ram.sv)
2. [ROM](./RTL/my_rom.sv)

### [Interface](./my_interface.sv)

Interface is needed for communication between the testbench and DUT blocks.

### [UVM Environment](./UVM/my_env.sv)

UVM environment consists of one or two [agents](./UVM/my_agent.sv) (depending on `TWO_MAPS` macro) and [UVM RAL environment](./UVM_RAL/my_ral_env.sv), i.e. environment with the register model.

In its build phase:
1. One or two agents are created based on a `TWO_MAPS` macro.
2. UVM RAL environment is created.

In its connect phase:
1. First agent's sequencer and adapter for default map are assigned to default address map of the register model.
2. Second agent's sequencer and adapter for the second map are assigned to the second address map of the register model if `TWO_MAPS` is defined.
3. First agent's monitor analysis port and predictor analysis port are connected.

### [UVM Agent](./UVM/my_agent.sv)

UVM agent consists of [sequencer](./UVM/my_sequencer.sv), [driver](./UVM/my_driver.sv) and [monitor](./UVM/my_monitor.sv).

Its build phase is used to create these three components and its connect phase is used to connect driver's and sequencer's ports with each other.

### [UVM Sequencer](./UVM/my_sequencer.sv)

Sequencer is used to pass bus transactions from sequences to the driver.

### [UVM Driver](./UVM/my_driver.sv)

Driver is used to convert bus transactions to logic-level signals for interface.

### [UVM Monitor](./UVM/my_monitor.sv)

Monitor is used to observe signals on the interface. If these signals form a valid read or write transaction on the registers, this transaction is sent to the monitor's subscribers via analysis port. If `TWO_MAPS` macro is defined - monitor does not send any data.

### [UVM RAL Environment](./UVM_RAL/my_ral_env.sv)

UVM RAL environment represents the whole register model. It consists of [register block](./UVM_RAL/Register_Model/my_ral_reg_block.sv), [adapter](./UVM_RAL/my_adapter.sv) for default map, [adapter](./UVM_RAL/my_second_adapter.sv) for the second map if `TWO_MAPS` is defined, and predictor.

In its build phase:
1. Aforementioned components are created (adapter for second map is created based on a `TWO_MAPS` macro).
2. Register model is built and locked.
3. Based on a `TWO_MAPS` macro implicit prediction is set for both maps or explicit prediction is set for a single default map.
4. HDL path for the register block is set.

In its connect phase:
1. Default map is assigned to predictor.
2. Adapter for default map is assigned to predictor.

### [UVM Adapter](./UVM_RAL/my_adapter.sv) and [UVM Second Adapter](./UVM_RAL/my_second_adapter.sv)

Both of these adapters are used to convert register layer transactions to bus transactions and vice versa. First adapter converts transactions based on offsets in default address map, whereas the second adapter converts transactions based on offsets in the second address map. Offsets are assigned in the [register block](./UVM_RAL/Register_Model/my_ral_reg_block.sv) by using `add_mem()` and `add_submap()` methods of a corresponding address map.

### [UVM Register Block](./UVM_RAL/Register_Model/my_ral_reg_block.sv)

Register block contains [file of RAL registers](./UVM_RAL/Register_Model/my_ral_reg_file.sv), [RAM](./UVM_RAL/Register_Model/Memories/my_ral_ram.sv) and [ROM](./UVM_RAL/Register_Model/Memories/my_ral_rom.sv).

In its build function:
1. Aforementioned RAL components are created, built, configured and added to the default address map of this register block.
2. If `TWO_MAPS` macro is defined, they are also added to the second address map of this register block.

### [UVM Register File](./UVM_RAL/Register_Model/my_ral_reg_file.sv)

Register file encapsulates [all RAL registers](./UVM_RAL/Register_Model/Registers) currently present in register model.

In its build phase:
1. All RAL registers are created, built and configured.
2. Their HDL paths are defined.
3. Registers are added to the default map and optionally to the second map (if `TWO_MAPS` macro is defined).

### [UVM RAL RAM](./UVM_RAL/Register_Model/Memories/my_ral_ram.sv) and [UVM RAL ROM](./UVM_RAL/Register_Model/Memories/my_ral_rom.sv)

In constructors of the memories we specify:
1. Number of memory locations (memory depth).
2. Number of bits in each memory location (memory width).
3. Memory access policy.
4. Which functional coverage models should be enabled for this memory.
5. Which cover groups are instantiated based on an enabled coverage models.

In their build functions we define HDL paths of the memories.

Memories also have implemented cover groups and `sample()` methods for coverage collection.

### [UVM RAL Registers](./UVM_RAL/Register_Model/Registers)

In constructor of each register we specify:
1. Total number of bits in this register.
2. Which functional coverage models should be enabled for this register.
3. Which cover groups are instantiated based on enabled coverage models.

In their build functions we configure each field of the register by specifying:
1. Parent register (current register).
2. Size in bits.
3. Position of its LSB relative to the LSB of the whole register.
4. Access policy.
5. Volatility.
6. Reset value.
7. Whether field is actually reset.
8. Whether field can be randomized.
9. Whether field is the only one to occupy a byte lane in the register.

Registers also have implemented cover groups and `sample()` methods for coverage collection.

## Prediction Configuration

### Implicit Prediction

To enable implicit prediction `uvm_reg_map::set_auto_predict(1)` should be called on a desired register address map. In our testbench it is done in [UVM RAL environment](./UVM_RAL/my_ral_env.sv).

### Explicit Prediction

To enable explicit prediction on a register address map following steps should be taken:
1. [UVM monitor](./UVM/my_monitor.sv) should be able to catch every transaction that is executed on registers in DUT.
2. Predictor (`uvm_reg_predictor`) should be instantiated, created and configured:
   1. Desired register address map should be assigned to a `map` variable of the predictor.
   2. Adapter of that register address map should be assigned to an `adapter` variable of the predictor.
   3. Monitor's analysis port and predictor's analysis port should be connected.

In our testbench steps 2.1 and 2.2 are done in [UVM RAL environment](./UVM_RAL/my_ral_env.sv), while step 2.3 is done in [UVM environment](./UVM/my_env.sv).

## Effects of TWO_MAPS macro

If `TWO_MAPS` macro is defined:
1. Second address map ([in register block](./UVM_RAL/Register_Model/my_ral_reg_block.sv)), its corresponding adapter ([in UVM RAL environment](./UVM_RAL/my_ral_env.sv)) and second agent ([in UVM environment](./UVM/my_env.sv)) are instantiated, created and configured.
2. Both default and second maps are configured to have implicit prediction enabled ([in UVM RAL environment](./UVM_RAL/my_ral_env.sv)).
3. [Monitors](./UVM/my_monitor.sv) do not send any values to their analysis ports.

If `TWO_MAPS` macro is not defined:
1. Second address map, its corresponding adapter and second agent are not instantiated.
2. Only one (default) address map is present in the register model and it is configured to have explicit prediction enabled.
3. Monitor sends caught transactions on the interface to the predictor.

## Coverage Collection

In order to properly set up automatic coverage collection for register block/register/memory we need to:
1. Specify which functional coverage models should be built in register blocks/registers/memories. It should be done before register model is built by utilizing `uvm_reg::include_coverage()`. In our testbench it is done in the [base test](./UVM/Tests/my_base_test.sv).
2. Register block/register/memory should be constructed (`new()`) with a desired coverage model by utilizing `build_coverage()` method.
3. Cover groups should be instantiated for specified coverage models based on a `has_coverage()` method.
4. Sample method(s) should sample values based on a `get_coverage()` method.
5. Before starting any sequence/test, sampling for desired coverage models should be enabled for desired register blocks/registers/memories by utilizing `set_coverage()` method. In our testbench it is done in the [tests](./UVM/Tests) before any of the sequences are started.

## UVM RAL Built-in Sequences

To execute built-in RAL sequence all we need to do is to
1. Assign our register model to a `model` variable of the sequence.
2. Start the sequence by calling its `start(NULL)` method.

Before any sequence is started, DUT should be reset. Test [my_built_in_sequences_test](./UVM/Tests/my_built_in_sequences_test.sv) contains all built-in RAL sequences, that can be executed on a register block.