`include "uvm_macros.svh"
import uvm_pkg::*;

// UVM register model generates READ and WRITE transactions using generic
// structure with type "uvm_reg_bus_op"
// UVM RAL Adapter is responsible for converting these "uvm_reg_bus_op" transactions to
// a specific bus transactions and vice versa.
class my_adapter extends uvm_reg_adapter;
  `uvm_object_utils (my_adapter)
  
  // Adapter needs to know the definition of bus item
  my_pkg::my_reg_model_item tx;
 
  function new (string name = "my_adapter");
    super.new (name);
  endfunction

  // This function converts register transaction to bus transaction
  // It accepts item of type "uvm_reg_bus_op" and creates and returns "uvm_sequence_item" item
  function uvm_sequence_item reg2bus(const ref uvm_reg_bus_op rw);  
    tx = my_pkg::my_reg_model_item::type_id::create("tx");
    
    tx.addr = rw.addr;
    // UVM_BURST_WRITE is used for burst_write() method for uvm_mem memories
    tx.write_enable = ((rw.kind == UVM_WRITE) || (rw.kind == UVM_BURST_WRITE)) ? 1 : 0;
    tx.data = rw.data;
    
    // Register transaction is supposed to read/write data from/to address rw.addr
    // We can deduce the target (registers, RAM or ROM) of current transactions
    // based on rw.addr[63:62] bits.
    //
    // Why? Because when we added registers, RAM and ROM memories to the register model address map
    // we set offsets for these registers and memories to the following values:
    // default_map.add_submap(ral_reg_file_inst.default_map, 0);
    // default_map.add_mem(ral_ram_inst, 64'h4000_0000_0000_0000, "RW");    
    // default_map.add_mem(ral_rom_inst, 64'h8000_0000_0000_0000, "RO");  
    case (rw.addr[63:62])
      2'b00 : begin
        tx.target = my_pkg::REGS;
      end
      
      2'b01 : begin
        tx.target = my_pkg::RAM;
      end
      
      2'b10 : begin
        tx.target = my_pkg::ROM;
      end
    endcase
    
    // Address is always pointing at a single byte,
    // In case of register transaction address of the first register will be 64'd0,
    // second - 64'd4, third - 64'd8 and so on
    // 
    // In case of memories memory transaction with offset 0 will have address 64'd0,
    // transaction with offset 1 will have address 64'd4, with offset 2 - 64'd8 and so on
    //
    // That is why we need to divide address by 4 (or logically shift 2 bits of the address to the right)
    tx.addr = tx.addr >> 2;
 
    return tx;
  endfunction
 
  // This function converts bus transaction to register transaction
  // It accepts item of type "uvm_sequence_item" and assigns values in "uvm_reg_bus_op" item
  function void bus2reg(uvm_sequence_item bus_item, ref uvm_reg_bus_op rw);
  
    // bus_item is of type "uvm_sequence_item", so we need to cast it to "my_reg_item" type
    assert( $cast(tx, bus_item) ) else begin
      `uvm_fatal("my_adapter", "Failed to cast item of type \"uvm_sequence_item\" to \"my_reg_item\"")
    end

    rw.addr = tx.addr << 2;
    rw.kind = tx.write_enable ? UVM_WRITE : UVM_READ;
    rw.data = tx.data;

    // Based on target of current bus transaction,
    // set rw.addr[63:62] bits accordingly
    case (tx.target)
      my_pkg::REGS : begin
        rw.addr[63:62] = 2'b00;
      end
      
      my_pkg::RAM : begin
        rw.addr[63:62] = 2'b01;
      end
      
      my_pkg::ROM : begin
        rw.addr[63:62] = 2'b10;
      end
    endcase

    rw.status = UVM_IS_OK;
  endfunction
endclass