// In Questa simulator you can encounter an error, where
// Questa can not see ROM_BYTES macro from my_pkg.sv package.
// In that case you need to specify the value of this macro manually here.
//
// ROM size in bytes
//`define ROM_BYTES 2**12

`include "uvm_macros.svh"
import uvm_pkg::*;

// UVM RAL ROM Memory
// Consists of READ-ONLY words ech 32 bits wide
// Number of words are defined with ROM_BYTES macro
class my_ral_rom extends uvm_mem;
  `uvm_object_utils(my_ral_rom)
  
  // Address offset for coverage collection
  uvm_reg_addr_t cov_offset;
  
  // Covergroup for memory addresses (read operations)
  covergroup rd_addr;
    // Save statistics for every instance of this covergroup separately
    option.per_instance = 1;
    
    // Check if read operation was executed on every word in memory
    rd_addr : coverpoint cov_offset {
      bins rd_addr[] = { [0:( ((`ROM_BYTES/4)-1) * 4 )] } with ( (item % 4) == 0 );
    }
  endgroup
  
  // Parameters:
  //   string name           - name of this instance of memory abstraction class
  //   longint unsigned size - total number of memory locations
  //   int unsigned n_bits   - total number of bits in each memory location
  //   string access = "RW"  - access policy of this memory ("RW" - RAM, "RO" - ROM)
  //   int has_coverage = UVM_NO_COVERAGE - which functional coverage models are present
  //                                        in the extension of the register abstraction class
  //
  // build_coverage():
  // We need to get the coverage model that was set in test,
  // so that we can set that coverage model for this memory.
  // We can do that using static method uvm_reg::build_coverage().
  //
  // Parameters:
  //   uvm_reg_cvr_t models - coverage model to compare against coverage model
  //                          from database
  //
  // locate a resource by scope {"uvm_reg::", get_full_name()} and name "include_coverage"
  // and compare its value against "models" coverage model.
  // Returns bitwise AND between "models" and coverage model from database.
  function new (string name = "my_ral_rom");
    super.new(name, `ROM_BYTES/4, 32, "RO", build_coverage(UVM_CVR_ALL));
    
    // Build covergroup for memory word addresses if
    // this memory was constructed with corresponding coverage model
    if (has_coverage(UVM_CVR_ADDR_MAP)) begin    
      rd_addr = new();
      rd_addr.set_inst_name({this.get_full_name(), ".rd_addr"});
    end
  endfunction
  
  // This method is invoked by the memory abstraction class whenever an address
  // within one of its address map is successfully read or written.
  // The specified offset is the offset within the memory, not an absolute address.
  virtual function void sample (uvm_reg_addr_t offset,
                                bit            is_read,
                                uvm_reg_map    map);
    // Sample address only when coverage sampling was enabled for
    // corresponding coverage model
    if (get_coverage(UVM_CVR_ADDR_MAP)) begin
      // Check if current operation is "read"
      if (is_read) begin
        cov_offset = offset;
        rd_addr.sample();
      end
    end
  endfunction
  
  function void build ();
    // In order to use backdoor operations with this memory
    // we need to specify HDL path to this memory
    // Parameters:
    //   string name         - Instance name of memory
    //   int offset          - Offset (bit) from which this particular instance of memory is implemented
    //   int size            - Number of bits that this memory implements
    //   bit first = 0       - If TRUE, starts the specification of a duplicate
    //                         HDL implementation of the memory
    //   string kind = "RTL" - Type of this register
    //
    // In this case HDL path to this memory will be "... .rom_inst.mem[i]", where i is an index
    // of a memory word
    // Notice that we do not need to specify HDL path as "rom_inst.mem[i]"!
    this.add_hdl_path_slice("rom_inst.mem", 0, (`ROM_BYTES/4) * 32);
  endfunction

endclass : my_ral_rom