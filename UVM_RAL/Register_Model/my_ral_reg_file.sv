`include "uvm_macros.svh"
import uvm_pkg::*;

// UVM RAL File of registers
// Consists of 32 registers each 32 bits wide:
// 1  READ-WRITE        - regs[0]
// 1  READ-CLEAR        - regs[1]
// 1  READ-SET          - regs[2]
// 1  WRITE-READ-CLEAR  - regs[3]
// 1  WRITE-READ-SET    - regs[4]
// 1  WRITE-CLEAR       - regs[5]
// 1  WRITE-SET         - regs[6]
// 1  WRITE-1-TOGGLE    - regs[7]
// 1  WRITE-0-TOGGLE    - regs[8]
// 7  READ-WRITE        - regs[9:15]
// 1  READ-ONLY         - regs[16]
// 1  RW/RO   (16b/16b) - regs[17]
// 1  RC/RS   (16b/16b) - regs[18]
// 1  WRC/WRS (16b/16b) - regs[19]
// 1  WC/WS   (16b/16b) - regs[20]
// 1  W1T/W0T (16b/16b) - regs[21]
// 10 READ-ONLY         - regs[22:31]
class my_ral_reg_file extends uvm_reg_block;
  `uvm_object_utils(my_ral_reg_file)
  
  rand my_pkg::my_ral_reg_rw      ral_reg_rw_inst [0:7];
  rand my_pkg::my_ral_reg_rc      ral_reg_rc_inst;
  rand my_pkg::my_ral_reg_rs      ral_reg_rs_inst;
  rand my_pkg::my_ral_reg_wrc     ral_reg_wrc_inst;
  rand my_pkg::my_ral_reg_wrs     ral_reg_wrs_inst;
  rand my_pkg::my_ral_reg_wc      ral_reg_wc_inst;
  rand my_pkg::my_ral_reg_ws      ral_reg_ws_inst;
  rand my_pkg::my_ral_reg_w1t     ral_reg_w1t_inst;
  rand my_pkg::my_ral_reg_w0t     ral_reg_w0t_inst;
  rand my_pkg::my_ral_reg_ro      ral_reg_ro_inst [0:10];
  rand my_pkg::my_ral_reg_rw_ro   ral_reg_rw_ro_inst;
  rand my_pkg::my_ral_reg_rc_rs   ral_reg_rc_rs_inst;
  rand my_pkg::my_ral_reg_wrc_wrs ral_reg_wrc_wrs_inst;
  rand my_pkg::my_ral_reg_wc_ws   ral_reg_wc_ws_inst;
  rand my_pkg::my_ral_reg_w1t_w0t ral_reg_w1t_w0t_inst;
  
  // Second address map
  `ifdef TWO_MAPS
    uvm_reg_map second_map;
  `endif
  
  // Address offset for coverage collection
  uvm_reg_addr_t cov_offset;
  
  // Covergroup for register addresses (write operations)
  covergroup wr_addr;
    // Save statistics for every instance of this covergroup separately
    option.per_instance = 1;
    
    // Check if write operation was executed on every one out of 32 registers
    wr_addr : coverpoint cov_offset {
      bins wr_addr[] = { [0:(31*4)] } with ( (item % 4) == 0 );
      
      // Ignore write operations on registers RC (regs[1]), RS (regs[2]), RO (regs[16]),
      //                                      RC/RS (regs[18]) and 10 RO (regs[22-31]). 
      ignore_bins ignore_addr = { 1*4,  2*4, 16*4, 18*4, 22*4, 23*4, 24*4,
                                 25*4, 26*4, 27*4, 28*4, 29*4, 30*4, 31*4};
    }
  endgroup
  
  // Covergroup for register addresses (read operations)
  covergroup rd_addr;
    // Save statistics for every instance of this covergroup separately
    option.per_instance = 1;
    
    // Check if read operation was executed on every one out of 32 registers
    rd_addr : coverpoint cov_offset {
      bins rd_addr[] = { [0:(31*4)] } with ( (item % 4) == 0 );
    }
  endgroup
 
  function new (string name = "my_ral_reg_file");
    // new():
    // Parameters:
    //   string name = ""                   - Name of this register block
    //   int has_coverage = UVM_NO_COVERAGE - Specifies which functional coverage models are
    //                                        present in the extension of the block
    //                                        abstraction class
    //
    // build_coverage():
    // We need to get the coverage model that was set in test,
    // so that we can set that coverage model for this register file.
    // We can do that using static method uvm_reg::build_coverage().
    //
    // Parameters:
    //   uvm_reg_cvr_t models - coverage model to compare against coverage model
    //                          from database
    //
    // locate a resource by scope {"uvm_reg::", get_full_name()} and name "include_coverage"
    // and compare its value against "models" coverage model.
    // Returns bitwise AND between "models" and coverage model from database.
    super.new(name, build_coverage(UVM_CVR_ALL));
    
    // Build covergroup for register addresses if
    // this register block was constructed with corresponding coverage model
    if (has_coverage(UVM_CVR_ADDR_MAP)) begin
      wr_addr = new();
      wr_addr.set_inst_name({this.get_full_name(), ".wr_addr"});
      
      rd_addr = new();
      rd_addr.set_inst_name({this.get_full_name(), ".rd_addr"});
    end
  endfunction
  
  // This method is invoked by the block abstraction class whenever an address
  // within one of its address map is successfully read or written.
  // The specified offset is the offset within the block, not an absolute address.
  virtual function void sample (uvm_reg_addr_t offset,
                                bit            is_read,
                                uvm_reg_map    map);
    // Sample address only when coverage sampling was enabled for
    // corresponding coverage model
    if (get_coverage(UVM_CVR_ADDR_MAP)) begin
      // Check if current operation is "read"
      if (is_read) begin
        cov_offset = offset;
        rd_addr.sample();
      end else if (!is_read) begin
        cov_offset = offset;
        wr_addr.sample();
      end
    end
  endfunction
  
  function void build ();
    // Create an address map for this register file
    // Parameters:
    //   string name              - Name of this address map
    //   uvm_reg_addr_t base_addr - The base address for the map.
    //                              All registers, memories, and sub-blocks within
    //                              the map will be at offsets to this address
    //   int unsigned n_bytes     - The byte-width of the bus on which this map is used
    //   uvm_endianness_e endian  - The endian format
    //   bit byte_addressing = 1  - Specifies whether consecutive addresses refer are
    //                              1 byte apart (TRUE) or n_bytes apart (FALSE).
    //                              Default is TRUE.
    //
    // default_map is of type "uvm_reg_map" and is used when no other
    // register map is explicitly specified for this block
    default_map = create_map("ral_reg_file_default_map", 0, 4, UVM_LITTLE_ENDIAN);
    
    // Create second map
    `ifdef TWO_MAPS
      second_map = create_map("ral_reg_file_second_map", 0, 4, UVM_LITTLE_ENDIAN);
    `endif
    
    //==================================================//
    //========================RW========================//
    //==================================================//
    
    // Create, build, configure and set HDL path for first RW register
    ral_reg_rw_inst[0] = my_pkg::my_ral_reg_rw::type_id::create("ral_reg_rw_inst_0");
    
    // Rename covergroup instances' names to be unique to avoid simulator warnings   
    if (ral_reg_rw_inst[0].has_coverage(UVM_CVR_REG_BITS)) begin
      ral_reg_rw_inst[0].wr_bits.set_inst_name({ral_reg_rw_inst[0].get_full_name(), ".wr_bits"});
      ral_reg_rw_inst[0].rd_bits.set_inst_name({ral_reg_rw_inst[0].get_full_name(), ".rd_bits"});
    end
    
    if (ral_reg_rw_inst[0].has_coverage(UVM_CVR_FIELD_VALS)) begin
      ral_reg_rw_inst[0].wr_vals.set_inst_name({ral_reg_rw_inst[0].get_full_name(), ".wr_vals"});
      ral_reg_rw_inst[0].rd_vals.set_inst_name({ral_reg_rw_inst[0].get_full_name(), ".rd_vals"});
    end
    
    ral_reg_rw_inst[0].build();
    
    // Instance-specific configuration of register
    // Parameters:
    //   uvm_reg_block blk_parent           - Parent block of this register
    //   uvm_reg_file regfile_parent = null - Parent register file for this register
    //   string hdl_path = ""               - HDL path to this register
    //
    // Here we set this register block as a parent for ral_reg_rw_inst_0[0] register
    ral_reg_rw_inst[0].configure(this);
    
    // In order to use backdoor operations with this register
    // we need to specify HDL path to this register
    // Parameters:
    //   string name         - Instance name of register
    //   int offset          - Offset of the LSB in the register that this variable implements
    //   int size            - Number of bits (toward the MSB) that this variable implements
    //   bit first = 0       - If TRUE, starts the specification of a duplicate HDL
    //                         implementation of the register.
    //   string kind = "RTL" - Type of this register
    //
    // In this case HDL path to this register will be "... .regs[0]"
    ral_reg_rw_inst[0].add_hdl_path_slice("regs[0]", 0, ral_reg_rw_inst[0].get_n_bits());
    
    // Finally we need to add register to default register map
    // Parameters:
    //   uvm_reg rg                         - Add the specified register instance rg to this address map
    //   uvm_reg_addr_t offset              - The register is located at the specified address offset
    //                                        from this map's configured base address
    //   string rights = "RW"               - The rights specify the register’s accessibility via this map.
    //                                        Valid values are “RW”, “RO”, and “WO”.
    //   bit unmapped = 0                   - If TRUE, the register does not occupy any physical addresses
    //                                        and the base address is ignored.
    //                                        Unmapped registers require a user-defined frontdoor to be specified.
    //   uvm_reg_frontdoor frontdoor = null - User-defined frontdoor access sequence
    default_map.add_reg(ral_reg_rw_inst[0], 0, "RW");
    
    // Add register to the second register map
    `ifdef TWO_MAPS
      second_map.add_reg(ral_reg_rw_inst[0], 0, "RW");
    `endif
    
    //==================================================//
    //========================RC========================//
    //==================================================//
    
    // Repeat the process for all other registers
    
    ral_reg_rc_inst = my_pkg::my_ral_reg_rc::type_id::create("ral_reg_rc_inst_1");
    
    if (ral_reg_rc_inst.has_coverage(UVM_CVR_REG_BITS)) begin
      ral_reg_rc_inst.rd_bits.set_inst_name({ral_reg_rc_inst.get_full_name(), ".rd_bits"});
    end
    
    if (ral_reg_rc_inst.has_coverage(UVM_CVR_FIELD_VALS)) begin
      ral_reg_rc_inst.rd_vals.set_inst_name({ral_reg_rc_inst.get_full_name(), ".rd_vals"});
    end
    
    ral_reg_rc_inst.build();
    ral_reg_rc_inst.configure(this);
    ral_reg_rc_inst.add_hdl_path_slice("regs[1]", 0, ral_reg_rc_inst.get_n_bits());
    default_map.add_reg(ral_reg_rc_inst, 4, "RO");
    
    `ifdef TWO_MAPS
      second_map.add_reg(ral_reg_rc_inst, 4, "RO");
    `endif
    
    //==================================================//
    //========================RS========================//
    //==================================================//
    
    ral_reg_rs_inst = my_pkg::my_ral_reg_rs::type_id::create("ral_reg_rs_inst_2");
    
    if (ral_reg_rs_inst.has_coverage(UVM_CVR_REG_BITS)) begin
      ral_reg_rs_inst.rd_bits.set_inst_name({ral_reg_rs_inst.get_full_name(), ".rd_bits"});
    end
    
    if (ral_reg_rs_inst.has_coverage(UVM_CVR_FIELD_VALS)) begin
      ral_reg_rs_inst.rd_vals.set_inst_name({ral_reg_rs_inst.get_full_name(), ".rd_vals"});
    end
    
    ral_reg_rs_inst.build();
    ral_reg_rs_inst.configure(this);
    ral_reg_rs_inst.add_hdl_path_slice("regs[2]", 0, ral_reg_rs_inst.get_n_bits());
    default_map.add_reg(ral_reg_rs_inst, 8, "RO");
    
    `ifdef TWO_MAPS
      second_map.add_reg(ral_reg_rs_inst, 8, "RO");
    `endif
    
    //==================================================//
    //========================WRC=======================//
    //==================================================//
    
    ral_reg_wrc_inst = my_pkg::my_ral_reg_wrc::type_id::create("ral_reg_wrc_inst_3");
    
    if (ral_reg_wrc_inst.has_coverage(UVM_CVR_REG_BITS)) begin
      ral_reg_wrc_inst.wr_bits.set_inst_name({ral_reg_wrc_inst.get_full_name(), ".wr_bits"});
      ral_reg_wrc_inst.rd_bits.set_inst_name({ral_reg_wrc_inst.get_full_name(), ".rd_bits"});
    end
    
    if (ral_reg_wrc_inst.has_coverage(UVM_CVR_FIELD_VALS)) begin
      ral_reg_wrc_inst.wr_vals.set_inst_name({ral_reg_wrc_inst.get_full_name(), ".wr_vals"});
      ral_reg_wrc_inst.rd_vals.set_inst_name({ral_reg_wrc_inst.get_full_name(), ".rd_vals"});
    end
    
    ral_reg_wrc_inst.build();
    ral_reg_wrc_inst.configure(this);
    ral_reg_wrc_inst.add_hdl_path_slice("regs[3]", 0, ral_reg_wrc_inst.get_n_bits());
    default_map.add_reg(ral_reg_wrc_inst, 12, "RW");
    
    `ifdef TWO_MAPS
      second_map.add_reg(ral_reg_wrc_inst, 12, "RW");
    `endif
    
    //==================================================//
    //========================WRS=======================//
    //==================================================//
    
    ral_reg_wrs_inst = my_pkg::my_ral_reg_wrs::type_id::create("ral_reg_wrs_inst_4");
    
    if (ral_reg_wrs_inst.has_coverage(UVM_CVR_REG_BITS)) begin
      ral_reg_wrs_inst.wr_bits.set_inst_name({ral_reg_wrs_inst.get_full_name(), ".wr_bits"});
      ral_reg_wrs_inst.rd_bits.set_inst_name({ral_reg_wrs_inst.get_full_name(), ".rd_bits"});
    end
    
    if (ral_reg_wrs_inst.has_coverage(UVM_CVR_FIELD_VALS)) begin
      ral_reg_wrs_inst.wr_vals.set_inst_name({ral_reg_wrs_inst.get_full_name(), ".wr_vals"});
      ral_reg_wrs_inst.rd_vals.set_inst_name({ral_reg_wrs_inst.get_full_name(), ".rd_vals"});
    end
    
    ral_reg_wrs_inst.build();
    ral_reg_wrs_inst.configure(this);
    ral_reg_wrs_inst.add_hdl_path_slice("regs[4]", 0, ral_reg_wrs_inst.get_n_bits());
    default_map.add_reg(ral_reg_wrs_inst, 16, "RW");
    
    `ifdef TWO_MAPS
      second_map.add_reg(ral_reg_wrs_inst, 16, "RW");
    `endif
    
    //==================================================//
    //========================WC========================//
    //==================================================//
    
    ral_reg_wc_inst = my_pkg::my_ral_reg_wc::type_id::create("ral_reg_wc_inst_5");
    
    if (ral_reg_wc_inst.has_coverage(UVM_CVR_REG_BITS)) begin
      ral_reg_wc_inst.rd_bits.set_inst_name({ral_reg_wc_inst.get_full_name(), ".rd_bits"});
    end
    
    if (ral_reg_wc_inst.has_coverage(UVM_CVR_FIELD_VALS)) begin
      ral_reg_wc_inst.rd_vals.set_inst_name({ral_reg_wc_inst.get_full_name(), ".rd_vals"});
    end
    
    ral_reg_wc_inst.build();
    ral_reg_wc_inst.configure(this);
    ral_reg_wc_inst.add_hdl_path_slice("regs[5]", 0, ral_reg_wc_inst.get_n_bits());
    default_map.add_reg(ral_reg_wc_inst, 20, "RW");
    
    `ifdef TWO_MAPS
      second_map.add_reg(ral_reg_wc_inst, 20, "RW");
    `endif
    
    //==================================================//
    //========================WS========================//
    //==================================================//
    
    ral_reg_ws_inst = my_pkg::my_ral_reg_ws::type_id::create("ral_reg_ws_inst_6");
    
    if (ral_reg_ws_inst.has_coverage(UVM_CVR_REG_BITS)) begin
      ral_reg_ws_inst.rd_bits.set_inst_name({ral_reg_ws_inst.get_full_name(), ".rd_bits"});
    end
    
    if (ral_reg_ws_inst.has_coverage(UVM_CVR_FIELD_VALS)) begin
      ral_reg_ws_inst.rd_vals.set_inst_name({ral_reg_ws_inst.get_full_name(), ".rd_vals"});
    end
    
    ral_reg_ws_inst.build();
    ral_reg_ws_inst.configure(this);
    ral_reg_ws_inst.add_hdl_path_slice("regs[6]", 0, ral_reg_ws_inst.get_n_bits());
    default_map.add_reg(ral_reg_ws_inst, 24, "RW");
    
    `ifdef TWO_MAPS
      second_map.add_reg(ral_reg_ws_inst, 24, "RW");
    `endif
    
    //==================================================//
    //========================W1T=======================//
    //==================================================//
    
    ral_reg_w1t_inst = my_pkg::my_ral_reg_w1t::type_id::create("ral_reg_w1t_inst_7");
    
    if (ral_reg_w1t_inst.has_coverage(UVM_CVR_REG_BITS)) begin
      ral_reg_w1t_inst.rd_bits.set_inst_name({ral_reg_w1t_inst.get_full_name(), ".rd_bits"});
      ral_reg_w1t_inst.wr_bits.set_inst_name({ral_reg_w1t_inst.get_full_name(), ".wr_bits"});
    end
    
    if (ral_reg_w1t_inst.has_coverage(UVM_CVR_FIELD_VALS)) begin
      ral_reg_w1t_inst.rd_vals.set_inst_name({ral_reg_w1t_inst.get_full_name(), ".rd_vals"});
    end
    
    ral_reg_w1t_inst.build();
    ral_reg_w1t_inst.configure(this);
    ral_reg_w1t_inst.add_hdl_path_slice("regs[7]", 0, ral_reg_w1t_inst.get_n_bits());
    default_map.add_reg(ral_reg_w1t_inst, 28, "RW");
    
    `ifdef TWO_MAPS
      second_map.add_reg(ral_reg_w1t_inst, 28, "RW");
    `endif
    
    //==================================================//
    //========================W0T=======================//
    //==================================================//
    
    ral_reg_w0t_inst = my_pkg::my_ral_reg_w0t::type_id::create("ral_reg_w0t_inst_8");
    
    if (ral_reg_w0t_inst.has_coverage(UVM_CVR_REG_BITS)) begin
      ral_reg_w0t_inst.rd_bits.set_inst_name({ral_reg_w0t_inst.get_full_name(), ".rd_bits"});
      ral_reg_w0t_inst.wr_bits.set_inst_name({ral_reg_w0t_inst.get_full_name(), ".wr_bits"});
    end
    
    if (ral_reg_w0t_inst.has_coverage(UVM_CVR_FIELD_VALS)) begin    
      ral_reg_w0t_inst.rd_vals.set_inst_name({ral_reg_w0t_inst.get_full_name(), ".rd_vals"});
    end
    
    ral_reg_w0t_inst.build();
    ral_reg_w0t_inst.configure(this);
    ral_reg_w0t_inst.add_hdl_path_slice("regs[8]", 0, ral_reg_w0t_inst.get_n_bits());
    default_map.add_reg(ral_reg_w0t_inst, 32, "RW");
    
    `ifdef TWO_MAPS
      second_map.add_reg(ral_reg_w0t_inst, 32, "RW");
    `endif
    
    //==================================================//
    //======================7 RW========================//
    //==================================================//
    
    for (int i = 1 ; i <= 7 ; ++i) begin
      ral_reg_rw_inst[i] = my_pkg::my_ral_reg_rw::type_id::create($sformatf("ral_reg_rw_inst_%0d", i + 8));
      
      if (ral_reg_rw_inst[i].has_coverage(UVM_CVR_REG_BITS)) begin
        ral_reg_rw_inst[i].wr_bits.set_inst_name({ral_reg_rw_inst[i].get_full_name(), ".wr_bits"});
        ral_reg_rw_inst[i].rd_bits.set_inst_name({ral_reg_rw_inst[i].get_full_name(), ".rd_bits"});
      end
    
      if (ral_reg_rw_inst[i].has_coverage(UVM_CVR_FIELD_VALS)) begin
        ral_reg_rw_inst[i].wr_vals.set_inst_name({ral_reg_rw_inst[i].get_full_name(), ".wr_vals"});
        ral_reg_rw_inst[i].rd_vals.set_inst_name({ral_reg_rw_inst[i].get_full_name(), ".rd_vals"});
      end
      
      ral_reg_rw_inst[i].build();
      ral_reg_rw_inst[i].configure(this);
      ral_reg_rw_inst[i].add_hdl_path_slice($sformatf("regs[%0d]", i + 8), 0, ral_reg_rw_inst[i].get_n_bits());
      default_map.add_reg(ral_reg_rw_inst[i], (4 * i) + 32, "RW");
      
      `ifdef TWO_MAPS
        second_map.add_reg(ral_reg_rw_inst[i], (4 * i) + 32, "RW");
      `endif
    end
    
    //==================================================//
    //========================RO========================//
    //==================================================//
    
    ral_reg_ro_inst[0] = my_pkg::my_ral_reg_ro::type_id::create("ral_reg_ro_inst_16");
    
    if (ral_reg_ro_inst[0].has_coverage(UVM_CVR_REG_BITS)) begin
      ral_reg_ro_inst[0].rd_bits.set_inst_name({ral_reg_ro_inst[0].get_full_name(), ".rd_bits"});
    end

    if (ral_reg_ro_inst[0].has_coverage(UVM_CVR_FIELD_VALS)) begin
      ral_reg_ro_inst[0].rd_vals.set_inst_name({ral_reg_ro_inst[0].get_full_name(), ".rd_vals"});
    end
    
    ral_reg_ro_inst[0].build();
    ral_reg_ro_inst[0].configure(this);
    ral_reg_ro_inst[0].add_hdl_path_slice("regs[16]", 0, ral_reg_ro_inst[0].get_n_bits());
    default_map.add_reg(ral_reg_ro_inst[0], 64, "RO");
    
    `ifdef TWO_MAPS
      second_map.add_reg(ral_reg_ro_inst[0], 64, "RO");
    `endif
    
    //==================================================//
    //========================RW/RO=====================//
    //==================================================//
    
    ral_reg_rw_ro_inst = my_pkg::my_ral_reg_rw_ro::type_id::create("ral_reg_rw_ro_inst_17");
    
    if (ral_reg_rw_ro_inst.has_coverage(UVM_CVR_REG_BITS)) begin
      ral_reg_rw_ro_inst.wr_bits.set_inst_name({ral_reg_rw_ro_inst.get_full_name(), ".wr_bits"});
      ral_reg_rw_ro_inst.rd_bits.set_inst_name({ral_reg_rw_ro_inst.get_full_name(), ".rd_bits"});
    end
    
    if (ral_reg_rw_ro_inst.has_coverage(UVM_CVR_FIELD_VALS)) begin
      ral_reg_rw_ro_inst.wr_vals.set_inst_name({ral_reg_rw_ro_inst.get_full_name(), ".wr_vals"});
      ral_reg_rw_ro_inst.rd_vals.set_inst_name({ral_reg_rw_ro_inst.get_full_name(), ".rd_vals"});
    end
    
    ral_reg_rw_ro_inst.build();
    ral_reg_rw_ro_inst.configure(this);
    ral_reg_rw_ro_inst.add_hdl_path_slice("regs[17]", 0, ral_reg_rw_ro_inst.get_n_bits());
    default_map.add_reg(ral_reg_rw_ro_inst, 68, "RW");
    
    `ifdef TWO_MAPS
      second_map.add_reg(ral_reg_rw_ro_inst, 68, "RW");
    `endif
    
    //==================================================//
    //========================RC/RS=====================//
    //==================================================//
    
    ral_reg_rc_rs_inst = my_pkg::my_ral_reg_rc_rs::type_id::create("ral_reg_rc_rs_inst_18");
    
    if (ral_reg_rc_rs_inst.has_coverage(UVM_CVR_REG_BITS)) begin
      ral_reg_rc_rs_inst.rd_bits.set_inst_name({ral_reg_rc_rs_inst.get_full_name(), ".rd_bits"});
    end
    
    if (ral_reg_rc_rs_inst.has_coverage(UVM_CVR_FIELD_VALS)) begin
      ral_reg_rc_rs_inst.rd_vals.set_inst_name({ral_reg_rc_rs_inst.get_full_name(), ".rd_vals"});
    end
    
    ral_reg_rc_rs_inst.build();
    ral_reg_rc_rs_inst.configure(this);
    ral_reg_rc_rs_inst.add_hdl_path_slice("regs[18]", 0, ral_reg_rc_rs_inst.get_n_bits());
    default_map.add_reg(ral_reg_rc_rs_inst, 72, "RO");
    
    `ifdef TWO_MAPS
      second_map.add_reg(ral_reg_rc_rs_inst, 72, "RO");
    `endif
    
    //==================================================//
    //========================WRC/WRS===================//
    //==================================================//
    
    ral_reg_wrc_wrs_inst = my_pkg::my_ral_reg_wrc_wrs::type_id::create("ral_reg_wrc_wrs_inst_19");
    
    if (ral_reg_wrc_wrs_inst.has_coverage(UVM_CVR_REG_BITS)) begin
      ral_reg_wrc_wrs_inst.wr_bits.set_inst_name({ral_reg_wrc_wrs_inst.get_full_name(), ".wr_bits"});
      ral_reg_wrc_wrs_inst.rd_bits.set_inst_name({ral_reg_wrc_wrs_inst.get_full_name(), ".rd_bits"});
    end
    
    if (ral_reg_wrc_wrs_inst.has_coverage(UVM_CVR_FIELD_VALS)) begin
      ral_reg_wrc_wrs_inst.wr_vals.set_inst_name({ral_reg_wrc_wrs_inst.get_full_name(), ".wr_vals"});
      ral_reg_wrc_wrs_inst.rd_vals.set_inst_name({ral_reg_wrc_wrs_inst.get_full_name(), ".rd_vals"});
    end
    
    ral_reg_wrc_wrs_inst.build();
    ral_reg_wrc_wrs_inst.configure(this);
    ral_reg_wrc_wrs_inst.add_hdl_path_slice("regs[19]", 0, ral_reg_wrc_wrs_inst.get_n_bits());
    default_map.add_reg(ral_reg_wrc_wrs_inst, 76, "RW");
    
    `ifdef TWO_MAPS
      second_map.add_reg(ral_reg_wrc_wrs_inst, 76, "RW");
    `endif
    
    //==================================================//
    //========================WC/WS=====================//
    //==================================================//
    
    ral_reg_wc_ws_inst = my_pkg::my_ral_reg_wc_ws::type_id::create("ral_reg_wc_ws_inst_20");
    
    if (ral_reg_wc_ws_inst.has_coverage(UVM_CVR_REG_BITS)) begin
      ral_reg_wc_ws_inst.rd_bits.set_inst_name({ral_reg_wc_ws_inst.get_full_name(), ".rd_bits"});
    end
    
    if (ral_reg_wc_ws_inst.has_coverage(UVM_CVR_FIELD_VALS)) begin
      ral_reg_wc_ws_inst.rd_vals.set_inst_name({ral_reg_wc_ws_inst.get_full_name(), ".rd_vals"});
    end
    
    ral_reg_wc_ws_inst.build();
    ral_reg_wc_ws_inst.configure(this);
    ral_reg_wc_ws_inst.add_hdl_path_slice("regs[20]", 0, ral_reg_wc_ws_inst.get_n_bits());
    default_map.add_reg(ral_reg_wc_ws_inst, 80, "RW");
    
    `ifdef TWO_MAPS
      second_map.add_reg(ral_reg_wc_ws_inst, 80, "RW");
    `endif
    
    //==================================================//
    //========================W1T/W0T===================//
    //==================================================//
    
    ral_reg_w1t_w0t_inst = my_pkg::my_ral_reg_w1t_w0t::type_id::create("ral_reg_w1t_w0t_inst_21");
    
    if (ral_reg_w1t_w0t_inst.has_coverage(UVM_CVR_REG_BITS)) begin
      ral_reg_w1t_w0t_inst.rd_bits.set_inst_name({ral_reg_w1t_w0t_inst.get_full_name(), ".rd_bits"});
      ral_reg_w1t_w0t_inst.wr_bits.set_inst_name({ral_reg_w1t_w0t_inst.get_full_name(), ".wr_bits"});
    end
    
    if (ral_reg_w1t_w0t_inst.has_coverage(UVM_CVR_FIELD_VALS)) begin
      ral_reg_w1t_w0t_inst.rd_vals.set_inst_name({ral_reg_w1t_w0t_inst.get_full_name(), ".rd_vals"});
    end
    
    ral_reg_w1t_w0t_inst.build();
    ral_reg_w1t_w0t_inst.configure(this);
    ral_reg_w1t_w0t_inst.add_hdl_path_slice("regs[21]", 0, ral_reg_w1t_w0t_inst.get_n_bits());
    default_map.add_reg(ral_reg_w1t_w0t_inst, 84, "RW");
    
    `ifdef TWO_MAPS
      second_map.add_reg(ral_reg_w1t_w0t_inst, 84, "RW");
    `endif
    
    //==================================================//
    //======================10 RO=======================//
    //==================================================//
    
    for (int i = 1 ; i <= 10 ; ++i) begin
      ral_reg_ro_inst[i] = my_pkg::my_ral_reg_ro::type_id::create($sformatf("ral_reg_ro_inst_%0d", i + 21));
      
      if (ral_reg_ro_inst[i].has_coverage(UVM_CVR_REG_BITS)) begin
        ral_reg_ro_inst[i].rd_bits.set_inst_name({ral_reg_ro_inst[i].get_full_name(), ".rd_bits"});
      end

      if (ral_reg_ro_inst[i].has_coverage(UVM_CVR_FIELD_VALS)) begin
        ral_reg_ro_inst[i].rd_vals.set_inst_name({ral_reg_ro_inst[i].get_full_name(), ".rd_vals"});
      end
      
      ral_reg_ro_inst[i].build();
      ral_reg_ro_inst[i].configure(this);
      ral_reg_ro_inst[i].add_hdl_path_slice($sformatf("regs[%0d]", i + 21), 0, ral_reg_ro_inst[i].get_n_bits());
      default_map.add_reg(ral_reg_ro_inst[i], (4 * i) + 84, "RO");
      
      `ifdef TWO_MAPS
        second_map.add_reg(ral_reg_ro_inst[i], (4 * i) + 84, "RO");
      `endif   
    end
    
  endfunction
endclass : my_ral_reg_file