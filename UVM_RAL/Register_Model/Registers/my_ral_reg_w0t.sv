`include "uvm_macros.svh"
import uvm_pkg::*;

// UVM RAL Register
// Consists of 32 bits with WRITE-0-TOGGLE permissions
class my_ral_reg_w0t extends uvm_reg;
  `uvm_object_utils(my_ral_reg_w0t)
    
  // This register's field
  rand uvm_reg_field reg_data;
  
  // Auxiliary variable for data to write coverage collection
  bit [31:0] cov_data_to_write;
  
  // Covergroup for writing individual register bits
  covergroup wr_bits;
    // Save statistics for every instance of this covergroup separately
    option.per_instance = 1;
    
    // We want to check if every bit of W0T field was set to '0',
    // i.e. we need to check if data to be written is one-cold value.
    //
    // We check if values fall within the interval [0:2**32-1],
    // but since default width of integers in SystemVerilog is 32 bits, we need to
    // extend operands in operation 2**32 to 33 bits.
    wr_bits : coverpoint cov_data_to_write {
      bins wr_bits[] = { [0:(33'd2**33'd32)-1] } with ( $onehot(~item) == 1 );
    }
  endgroup
  
  // Covergroup for reading individual register bits
  covergroup rd_bits;
    // Save statistics for every instance of this covergroup separately
    option.per_instance = 1;
    
    // We want to check if every bit of W0T field was set to '1' (via backdoor),
    // and then was read, i.e. we need to check if current value in the W0T field during
    // read operation is one-cold value.
    //
    // We check if values fall within the interval [0:2**32-1],
    // but since default width of integers in SystemVerilog is 32 bits, we need to
    // extend operands in operation 2**32 to 33 bits.
    rd_bits : coverpoint reg_data.value[31:0] {
      bins rd_bits[] = { [0:(33'd2**33'd32)-1] } with ( $onehot(~item) == 1 );
    }
  endgroup
  
  // Covergroup for reading field values
  covergroup rd_vals;
    // Save statistics for every instance of this covergroup separately
    option.per_instance = 1;
    
    // Distribute all possible values between 4 bins, i.e. check if
    // current value of the register falls within the interval [0:2**32-1]
    rd_vals : coverpoint reg_data.value[31:0] {
      bins rd_vals[4] = { [0:(33'd2**33'd32)-1] };
    }
  endgroup

  function new (string name = "my_ral_reg_w0t");
    // new():
    // Parameters:
    //   string name = ""    - Name of this register
    //   int unsigned n_bits - Total number of bits in the register
    //   int has_coverage    - Specifies which functional coverage models are
    //                         present in the extension of the register
    //                         abstraction class
    //
    // build_coverage():
    // We need to get the coverage model that was set in test,
    // so that we can set that coverage model for this register.
    // We can do that using static method uvm_reg::build_coverage().
    //
    // Parameters:
    //   uvm_reg_cvr_t models - coverage model to compare against coverage model
    //                          from database
    //
    // locate a resource by scope {"uvm_reg::", get_full_name()} and name "include_coverage"
    // and compare its value against "models" coverage model.
    // Returns bitwise AND between "models" and coverage model from database.
    super.new(name, 32, build_coverage(UVM_CVR_ALL));
    
    // Build covergroup for individual register bits if
    // this register was constructed with corresponding coverage model
    if (has_coverage(UVM_CVR_REG_BITS)) begin
      rd_bits = new();
      wr_bits = new();
    end
    
    // Build covergroup for field values if
    // this register was constructed with corresponding coverage model 
    if (has_coverage(UVM_CVR_FIELD_VALS)) begin    
      rd_vals = new();
    end
  endfunction
  
  // This method is invoked by the register abstraction class whenever it is
  // read or written with the specified "data" via the specified address "map"
  virtual function void sample (uvm_reg_data_t data,
                                uvm_reg_data_t byte_en,
                                bit            is_read,
                                uvm_reg_map    map);
    // Sample values only when coverage sampling was enabled for
    // corresponding coverage model
    if (get_coverage(UVM_CVR_REG_BITS)) begin
      // Check if current operation is "read"
      if (is_read) begin
        rd_bits.sample();
      end else if (!is_read) begin
        cov_data_to_write = data;
        wr_bits.sample();
      end
    end
    
    if (get_coverage(UVM_CVR_FIELD_VALS)) begin
      // Check if current operation is "read"
      if (is_read) begin
        rd_vals.sample();
      end
    end
  endfunction
 
  function void build ();
    reg_data = uvm_reg_field::type_id::create("reg_data");
  
    // Parameters:
    //   uvm_reg parent              - Parent register of the field
    //   int unsigned size           - Size in bits
    //   int unsigned lsb_pos        - Position of the least-significant bit
    //                                 relative to the least-significant bit of the register
    //   string access               - Access policy
    //   bit volatile                - Volatility
    //   uvm_reg_data_t reset        - "HARD" reset value
    //   bit has_reset               - Is field value actually reset?
    //                                 (the reset value is ignored if FALSE)
    //   bit is_rand                 - Can value be randomized?
    //   bit individually_accessible - Is field the only one to occupy a byte lane in the register?
    reg_data.configure(this, 32, 0, "W0T", 0, 0, 1, 1, 1);
  endfunction
endclass : my_ral_reg_w0t