`include "uvm_macros.svh"
import uvm_pkg::*;

// UVM RAL Register
// Consists of 16 bits with WRITE-READ-CLEAR permissions and
//             16 bits with WRITE-READ-SET permissions
class my_ral_reg_wrc_wrs extends uvm_reg;
  `uvm_object_utils(my_ral_reg_wrc_wrs)
    
  // This register's WRC field
  rand uvm_reg_field reg_data_wrc;
  
  // This register's WRS field
  rand uvm_reg_field reg_data_wrs;
  
  // Covergroup for writing individual register bits
  covergroup wr_bits;
    // Save statistics for every instance of this covergroup separately
    option.per_instance = 1;
    
    // We want to check if every bit of WRC field was set to '1',
    // i.e. we need to check if current value in the WRC field during
    // write operation is one-hot value.
    wr_bits_wrc : coverpoint reg_data_wrc.value[15:0] {
      bins wr_bits_wrc[] = { [0:(2**16)-1] } with ( $onehot(item) == 1 );
    }
    
    // Same thing for WRS field (with one-cold value)
    wr_bits_wrs : coverpoint reg_data_wrs.value[15:0] {
      bins wr_bits_wrs[] = { [0:(2**16)-1] } with ( $onehot(~item) == 1 );
    }
  endgroup
  
  // Covergroup for reading individual register bits
  covergroup rd_bits;
    // Save statistics for every instance of this covergroup separately
    option.per_instance = 1;
    
    // We want to check if every bit of WRC field was read
    rd_bits_wrc : coverpoint reg_data_wrc.value[15:0] {
      bins rd_bits_wrc[] = { [0:(2**16)-1] } with ( $onehot(item) == 1 );
    }
    
    // Same thing for WRS field (with one-cold value)
    rd_bits_wrs : coverpoint reg_data_wrs.value[15:0] {
      bins rd_bits_wrs[] = { [0:(2**16)-1] } with ( $onehot(~item) == 1 );
    }
  endgroup
  
  // Covergroup for writing field values
  covergroup wr_vals;
    // Save statistics for every instance of this covergroup separately
    option.per_instance = 1;
    
    // Distribute all possible values between 4 bins, i.e. check if
    // current value of the WRC field falls within the interval [0:2**16-1]
    wr_vals_wrc : coverpoint reg_data_wrc.value[15:0] {
      bins wr_vals_wrc[4] = { [0:(2**16)-1] };
    }
    
    // Same thing for WRS field
    wr_vals_wrs : coverpoint reg_data_wrs.value[15:0] {
      bins wr_vals_wrs[4] = { [0:(2**16)-1] };
    }
  endgroup
  
  // Covergroup for reading field values
  covergroup rd_vals;
    // Save statistics for every instance of this covergroup separately
    option.per_instance = 1;
    
    // Distribute all possible values between 4 bins, i.e. check if
    // current value of the WRC field falls within the interval [1:2**16-1].
    //
    // Create an explicit bin for zero value.
    rd_vals_wrc : coverpoint reg_data_wrc.value[15:0] {
      bins rd_vals_wrc[4] = { [1:(2**16)-1] };
      bins zeroes_wrc     = {0};
    }
    
    // Same thing for WRS field.
    // Create an explicit bin for max value (all bits are '1').
    rd_vals_wrs : coverpoint reg_data_wrs.value[15:0] {
      bins rd_vals_wrs[4] = { [0:(2**16)-2] };
      bins ones_wrs       = { (2**16)-1 };
    }
  endgroup

  function new (string name = "my_ral_reg_wrc_wrs");
    // new():
    // Parameters:
    //   string name = ""    - Name of this register
    //   int unsigned n_bits - Total number of bits in the register
    //   int has_coverage    - Specifies which functional coverage models are
    //                         present in the extension of the register
    //                         abstraction class
    //
    // build_coverage():
    // We need to get the coverage model that was set in test,
    // so that we can set that coverage model for this register.
    // We can do that using static method uvm_reg::build_coverage().
    //
    // Parameters:
    //   uvm_reg_cvr_t models - coverage model to compare against coverage model
    //                          from database
    //
    // locate a resource by scope {"uvm_reg::", get_full_name()} and name "include_coverage"
    // and compare its value against "models" coverage model.
    // Returns bitwise AND between "models" and coverage model from database.
    super.new(name, 32, build_coverage(UVM_CVR_ALL));
    
    // Build covergroup for individual register bits if
    // this register was constructed with corresponding coverage model
    if (has_coverage(UVM_CVR_REG_BITS)) begin
      wr_bits = new();
      rd_bits = new();
    end
    
    // Build covergroup for field values if
    // this register was constructed with corresponding coverage model 
    if (has_coverage(UVM_CVR_FIELD_VALS)) begin
      wr_vals = new();
      rd_vals = new();
    end
  endfunction
  
  // This method is invoked by the register abstraction class whenever it is
  // read or written with the specified "data" via the specified address "map"
  virtual function void sample (uvm_reg_data_t data,
                                uvm_reg_data_t byte_en,
                                bit            is_read,
                                uvm_reg_map    map);
    // Sample values only when coverage sampling was enabled for
    // corresponding coverage model
    if (get_coverage(UVM_CVR_REG_BITS)) begin
      // Check if current operation is "read"
      if (is_read) begin
        rd_bits.sample();
      end else if (!is_read) begin
        wr_bits.sample();
      end
    end
    
    if (get_coverage(UVM_CVR_FIELD_VALS)) begin
      // Check if current operation is "read"
      if (is_read) begin
        rd_vals.sample();
      end else if (!is_read) begin
        wr_vals.sample();
      end
    end
  endfunction
 
  function void build ();
    reg_data_wrc = uvm_reg_field::type_id::create("reg_data_wrc");
    reg_data_wrs = uvm_reg_field::type_id::create("reg_data_wrs");
  
    // Parameters:
    //   uvm_reg parent              - Parent register of the field
    //   int unsigned size           - Size in bits
    //   int unsigned lsb_pos        - Position of the least-significant bit
    //                                 relative to the least-significant bit of the register
    //   string access               - Access policy
    //   bit volatile                - Volatility
    //   uvm_reg_data_t reset        - "HARD" reset value
    //   bit has_reset               - Is field value actually reset?
    //                                 (the reset value is ignored if FALSE)
    //   bit is_rand                 - Can value be randomized?
    //   bit individually_accessible - Is field the only one to occupy a byte lane in the register?
    reg_data_wrs.configure(this, 16, 0, "WRS", 0, 0, 1, 1, 1);
    reg_data_wrc.configure(this, 16, 16, "WRC", 0, 0, 1, 1, 1);
  endfunction
endclass : my_ral_reg_wrc_wrs