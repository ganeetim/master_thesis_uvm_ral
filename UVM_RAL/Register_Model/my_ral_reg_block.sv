`include "uvm_macros.svh"
import uvm_pkg::*;

// UVM RAL Register block
// Consists of one register file, RAM and ROM memories
class my_ral_reg_block extends uvm_reg_block;
  `uvm_object_utils(my_ral_reg_block)
  
  rand my_pkg::my_ral_reg_file ral_reg_file_inst;
  rand my_pkg::my_ral_ram ral_ram_inst;
  rand my_pkg::my_ral_rom ral_rom_inst;
  
  // Second address map
  `ifdef TWO_MAPS
    uvm_reg_map second_map;
  `endif
 
  function new (string name = "my_ral_reg_block");
    // new():
    // Parameters:
    //   string name = ""                   - Name of this register block
    //   int has_coverage = UVM_NO_COVERAGE - Specifies which functional coverage models are
    //                                        present in the extension of the block
    //                                        abstraction class
    //
    // build_coverage():
    // We need to get the coverage model that was set in test,
    // so that we can set that coverage model for this register block.
    // We can do that using static method uvm_reg::build_coverage().
    //
    // Parameters:
    //   uvm_reg_cvr_t models - coverage model to compare against coverage model
    //                          from database
    //
    // locate a resource by scope {"uvm_reg::", get_full_name()} and name "include_coverage"
    // and compare its value against "models" coverage model.
    // Returns bitwise AND between "models" and coverage model from database.
    super.new(name, build_coverage(UVM_CVR_ALL));
  endfunction
  
  function void build ();
    // Create an address map for this register block
    default_map = create_map("ral_reg_block_default_map", 0, 4, UVM_LITTLE_ENDIAN);
    
    // Create, build, configure and set HDL path for this register file
    ral_reg_file_inst = my_pkg::my_ral_reg_file::type_id::create("ral_reg_file_inst");
    ral_reg_file_inst.build();
    
    // Sets HDL path for this register file
    // Parameters:
    //   uvm_reg_block parent = null - Specify the parent block of this block
    //   string hdl_path = ""        - HDL path to this block
    //
    // In this case HDL path of this block will be "... .regs_inst. ..."
    ral_reg_file_inst.configure(this, "regs_inst");
    
    // Add register file map to the register block map
    // Parameters:
    //   uvm_reg_map child_map - Address map to be added to the map of the current block
    //   uvm_reg_addr_t offset - Offset to that child map
    default_map.add_submap(ral_reg_file_inst.default_map, 0);
    
    // Create, build, configure and set HDL path for RAM memory
    ral_ram_inst = my_pkg::my_ral_ram::type_id::create("ral_ram_inst");
    ral_ram_inst.build();
    
    // Parameters:
    //   uvm_reg_block parent - Specify the parent block of this memory
    //   string hdl_path = "" - HDL path to this memory
    //
    // Notice that we do not need to specify HDL path here, since we already did that
    // in the RAM itself
    ral_ram_inst.configure(this);
    
    // Add RAM memory to the register block map
    // Parameters:
    //   uvm_mem mem                        - Add the specified memory instance to this address map
    //   uvm_reg_addr_t offset              - Offset to that memory (every transaction executed
    //                                        on this memory will have address with this offset in mind)
    //   string rights = "RW"               - Access rights to this memory
    //   bit unmapped = 0                   - If TRUE, the memory does not occupy any physical addresses
    //                                        and the base address is ignored
    //   uvm_reg_frontdoor frontdoor = null - Unmapped memories require a user-defined
    //                                        frontdoor to be specified
    //
    // We add such offset so that we can differentiate between register and memory operations
    default_map.add_mem(ral_ram_inst, 64'h4000_0000_0000_0000, "RW");
    
    // Do the same steps for ROM memory
    ral_rom_inst = my_pkg::my_ral_rom::type_id::create("ral_rom_inst");
    ral_rom_inst.build();
    ral_rom_inst.configure(this);
    default_map.add_mem(ral_rom_inst, 64'h8000_0000_0000_0000, "RO");
    
    // Add register file, RAM and ROM memories to the register block second map.
    // Offsets are different, than in the default register map.
    // It is done that way to showcase that both maps would refer to the same
    // register/RAM/ROM element, even though their offsets in the maps are different.   
    `ifdef TWO_MAPS
      second_map = create_map("ral_reg_block_second_map", 0, 4, UVM_LITTLE_ENDIAN);
      second_map.add_mem(ral_rom_inst, 0, "RO");
      second_map.add_submap(ral_reg_file_inst.second_map, 64'h4000_0000_0000_0000);
      second_map.add_mem(ral_ram_inst, 64'h8000_0000_0000_0000, "RW");
    `endif

  endfunction
endclass : my_ral_reg_block