`include "uvm_macros.svh"
import uvm_pkg::*;

// UVM RAL Environment consists of UVM RAL Adapter and Register block
class my_ral_env extends uvm_env;
  
  // Register block
  my_pkg::my_ral_reg_block ral_reg_block_inst;
	
  // UVM RAL Adapter
  my_pkg::my_adapter default_map_adapter_inst;
  
  // UVM RAL Adapter for the second register map
  `ifdef TWO_MAPS
    my_pkg::my_second_adapter second_map_adapter_inst;
  `endif
  
  // UVM RAL Predictor
  uvm_reg_predictor #(my_pkg::my_reg_model_item) predictor_inst;
  
  `uvm_component_utils_begin(my_ral_env)
    `uvm_field_object(ral_reg_block_inst, UVM_ALL_ON)
  `uvm_component_utils_end
  
  function new (string name = "my_ral_env", uvm_component parent = null);
    super.new(name, parent);
  endfunction
  
  function void build_phase (uvm_phase phase);
    super.build_phase(phase);

    ral_reg_block_inst = my_pkg::my_ral_reg_block::type_id::create("ral_reg_block_inst", this);
    default_map_adapter_inst = my_pkg::my_adapter::type_id::create("default_map_adapter_inst", this);
    
    `ifdef TWO_MAPS
      second_map_adapter_inst = my_pkg::my_second_adapter::type_id::create("second_map_adapter_inst", this);
    `endif
    
    predictor_inst = uvm_reg_predictor#(my_pkg::my_reg_model_item)::type_id::create("predictor_inst", this);
	  
    // Build register model
    ral_reg_block_inst.build();
    
    // Lock a model and build the address map.
    // Recursively lock an entire register model and build the address maps.
    // Once locked, no further structural changes, such as adding registers or memories, can be made.
    // It is not possible to unlock a model.
    ral_reg_block_inst.lock_model();
    
    // We need to set the auto-predict mode for this map
    // Parameters:
    //   bit on = 1
    //
    // When on is TRUE, the register model will automatically update its mirror
    // (what it thinks should be in the DUT) immediately after any bus read or write operation via this map.
    // When on is FALSE, bus reads and writes via this map do not automatically update the mirror.
    // By default, auto-prediction is turned off.
    //
    // In case of having two register maps in the register block we need to
    // turn auto-prediction ON for both maps.
    // It is needed, because in case of both maps having explicit prediction,
    // both maps would monitor the same physical interface and therefore
    // mirrored and desired values in register model would contain wrong values.
    // That is why we need to make both maps "blind" to what happens on that interface,
    // i.e. set them to have implicit prediction.
    `ifdef TWO_MAPS
      ral_reg_block_inst.default_map.set_auto_predict(1);
      ral_reg_block_inst.second_map.set_auto_predict(1);
    `else
      ral_reg_block_inst.default_map.set_auto_predict(0);
    `endif
    
    // Set root HDL path for this register block
    // Parameters:
    //   string path         - absolute HDL path to the block instance for the specified design abstraction
    //   string kind = "RTL" - Type of this block
    //
    // This absolute root path is prepended to all hierarchical paths under this block.
    // In this case all HDL paths under this block will start with "uvm_ral_top. ..."
    ral_reg_block_inst.set_hdl_path_root("uvm_ral_top");
  endfunction
  
  function void connect_phase (uvm_phase phase);
    super.connect_phase(phase);
    
    // Predictor needs to know what map will be used to convert a bus address
    // to corresponding register or memory word
    predictor_inst.map = ral_reg_block_inst.default_map;
    
    // Predictor needs a handle to an adapter, which will be used
    // to convert bus transactions to register model transactions
    predictor_inst.adapter = default_map_adapter_inst;

  endfunction

endclass