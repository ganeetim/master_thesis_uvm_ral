package my_pkg;
  
  // RAM size in bytes
  `define RAM_BYTES 2**12
  
  // ROM size in bytes
  `define ROM_BYTES 2**12
  
  // Target of current operation (registers, RAM or ROM)
  // Used in my_reg_model_item.sv
  typedef enum bit [1:0] {REGS = 2'b00, RAM = 2'b01, ROM = 2'b10} target_type;

  // UVM
  `include "./UVM/my_reg_model_item.sv"
  `include "./UVM/my_driver.sv"
  `include "./UVM/my_sequencer.sv"
  `include "./UVM/my_monitor.sv"
  `include "./UVM/my_agent.sv"
  
  // UVM RAL
  //   Registers:
  `include "./UVM_RAL/Register_Model/Registers/my_ral_reg_rw.sv"
  `include "./UVM_RAL/Register_Model/Registers/my_ral_reg_rc.sv"
  `include "./UVM_RAL/Register_Model/Registers/my_ral_reg_rs.sv"
  `include "./UVM_RAL/Register_Model/Registers/my_ral_reg_wrc.sv"
  `include "./UVM_RAL/Register_Model/Registers/my_ral_reg_wrs.sv"
  `include "./UVM_RAL/Register_Model/Registers/my_ral_reg_wc.sv"
  `include "./UVM_RAL/Register_Model/Registers/my_ral_reg_ws.sv"
  `include "./UVM_RAL/Register_Model/Registers/my_ral_reg_w1t.sv"
  `include "./UVM_RAL/Register_Model/Registers/my_ral_reg_w0t.sv"
  `include "./UVM_RAL/Register_Model/Registers/my_ral_reg_ro.sv"
  `include "./UVM_RAL/Register_Model/Registers/my_ral_reg_rw_ro.sv"
  `include "./UVM_RAL/Register_Model/Registers/my_ral_reg_rc_rs.sv"
  `include "./UVM_RAL/Register_Model/Registers/my_ral_reg_wrc_wrs.sv"
  `include "./UVM_RAL/Register_Model/Registers/my_ral_reg_wc_ws.sv"
  `include "./UVM_RAL/Register_Model/Registers/my_ral_reg_w1t_w0t.sv"
  //   Memories:
  `include "./UVM_RAL/Register_Model/Memories/my_ral_ram.sv"
  `include "./UVM_RAL/Register_Model/Memories/my_ral_rom.sv"
  
  `include "./UVM_RAL/Register_Model/my_ral_reg_file.sv"
  `include "./UVM_RAL/Register_Model/my_ral_reg_block.sv"
  `include "./UVM_RAL/my_adapter.sv"
  `include "./UVM_RAL/my_second_adapter.sv"
  `include "./UVM_RAL/my_ral_env.sv"
  
  // UVM Environment
  `include "./UVM/my_env.sv"
  
  // Sequences
  //   Reset_Sequence:
   `include "./UVM/Sequences/my_reset_seq.sv"
  //   Memory_Sequences:
  `include "./UVM/Sequences/Memory_Sequences/my_mem_ram_read_write_seq.sv"
  `include "./UVM/Sequences/Memory_Sequences/my_mem_rom_read_write_seq.sv"
  `include "./UVM/Sequences/Memory_Sequences/my_mem_burst_seq.sv"
  
  //   Register_Sequences:
  `include "./UVM/Sequences/Register_Sequences/my_reg_rw_manual_seq.sv"
  `include "./UVM/Sequences/Register_Sequences/my_reg_rw_mirror_seq.sv"
  `include "./UVM/Sequences/Register_Sequences/my_reg_ro_manual_seq.sv"
  `include "./UVM/Sequences/Register_Sequences/my_reg_ro_mirror_seq.sv"
  `include "./UVM/Sequences/Register_Sequences/my_reg_desired_seq.sv"
  `include "./UVM/Sequences/Register_Sequences/my_reg_mirrored_seq.sv"
  `include "./UVM/Sequences/Register_Sequences/my_reg_exp_prediction.sv"
  `include "./UVM/Sequences/Register_Sequences/my_all_regs_0_1_seq.sv"
  
  // Tests
  `include "./UVM/Tests/my_base_test.sv"
  `include "./UVM/Tests/my_custom_sequences_test.sv"
  `include "./UVM/Tests/my_built_in_sequences_test.sv"
  `include "./UVM/Tests/my_full_test.sv"
   
endpackage