// In Questa simulator you can encounter an error, where
// Questa can not see RAM_BYTES macro from my_pkg.sv package.
// In that case you need to specify the value of this macro manually here.
//
// RAM size in bytes
//`define RAM_BYTES 2**12

module my_ram
(
  input  logic clk,
  input  logic reset,
  input  logic [31:0] ram_addr,
  input  logic write_enable,
  input  logic [31:0] ram_in,
  output logic [31:0] ram_out
);

  logic [31:0] mem [0:(`RAM_BYTES/4)-1];

  assign ram_out = mem[ram_addr];
  
  always @(posedge clk) begin
    if (reset === 1'b0) begin
      foreach (mem[i]) begin
        mem[i] <= 32'h0000_0000;
      end
    end else begin
      if (write_enable === 1'b1) begin
        `ifdef RAM_BUGS
          if (ram_addr == 0) begin
            mem[ram_addr] <= ram_in + 1;
          end else if (ram_addr == (`RAM_BYTES/4)/2) begin
            mem[ram_addr] <= 32'hABCD_DCBA;
          end else if (ram_addr != (`RAM_BYTES/4)-1) begin
            mem[ram_addr] <= ram_in;
          end
        `else
          mem[ram_addr] <= ram_in;
        `endif
      end
    end
  end
  
endmodule