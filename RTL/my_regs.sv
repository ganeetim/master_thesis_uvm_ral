// Collection of 32 registers each 32 bits wide:
// 1  READ-WRITE        - regs[0]
// 1  READ-CLEAR        - regs[1]
// 1  READ-SET          - regs[2]
// 1  WRITE-READ-CLEAR  - regs[3]
// 1  WRITE-READ-SET    - regs[4]
// 1  WRITE-CLEAR       - regs[5]
// 1  WRITE-SET         - regs[6]
// 1  WRITE-1-TOGGLE    - regs[7]
// 1  WRITE-0-TOGGLE    - regs[8]
// 7  READ-WRITE        - regs[9:15]
// 1  READ-ONLY         - regs[16]
// 1  RW/RO   (16b/16b) - regs[17]
// 1  RC/RS   (16b/16b) - regs[18]
// 1  WRC/WRS (16b/16b) - regs[19]
// 1  WC/WS   (16b/16b) - regs[20]
// 1  W1T/W0T (16b/16b) - regs[21]
// 10 READ-ONLY         - regs[22:31]
module my_regs
(
  input  logic clk,
  input  logic reset,
  input  logic [31:0] reg_addr,
  input  logic write_enable,
  input  logic [31:0] reg_in,
  output logic [31:0] reg_out
);

  logic [31:0] regs [0:31];

  assign reg_out = regs[reg_addr];
	
  always @(posedge clk) begin
    if (reset === 1'b0) begin
      foreach (regs[i]) begin
        regs[i] <= 32'h0000_0000;
        `ifdef REG_BUGS
          regs[5] <= 32'h0010_0000;
        `endif
      end
    end else begin
      //==================================================//
      // WRITE OPERATION
      //==================================================//
      
      if (write_enable === 1'b1) begin
        // 1 RW Register - regs[0]
        if (reg_addr === 32'd0) begin
          `ifdef REG_BUGS
            regs[reg_addr] <= reg_in + 1;
          `else
            regs[reg_addr] <= reg_in;
          `endif
        end
        
        // 1 RC Register - regs[1]
        
        // 1 RS Register - regs[2]
        
        // 1 WRC Register - regs[3]
        if (reg_addr === 32'd3) begin
          `ifdef REG_BUGS
            regs[reg_addr][15:0] <= reg_in;
          `else
            regs[reg_addr] <= reg_in;
          `endif
        end
        
        // 1 WRS Register - regs[4]
        if (reg_addr === 32'd4) begin
          `ifdef REG_BUGS
            regs[reg_addr][31:16] <= reg_in;
          `else
            regs[reg_addr] <= reg_in;
          `endif
        end
        
        // 1 WC Register - regs[5]
        if (reg_addr === 32'd5) begin
          `ifdef REG_BUGS
            regs[reg_addr] <= 32'h8000_0000;
          `else
            regs[reg_addr] <= 32'h0000_0000;
          `endif
        end
        
        // 1 WS Register - regs[6]
        if (reg_addr === 32'd6) begin
          `ifdef REG_BUGS
            regs[reg_addr] <= reg_in;
          `else
            regs[reg_addr] <= 32'hFFFF_FFFF;
          `endif
        end
        
        // 1 W1T Register - regs[7]
        if (reg_addr === 32'd7) begin
          `ifdef REG_BUGS
            regs[reg_addr] <= regs[reg_addr] ^ (~reg_in);
          `else
            regs[reg_addr] <= regs[reg_addr] ^ reg_in;
          `endif
        end
        
        // 1 W0T Register - regs[8]
        if (reg_addr === 32'd8) begin
          `ifdef REG_BUGS
            regs[reg_addr] <= regs[reg_addr] ^ ((~reg_in) + 1);
          `else
            regs[reg_addr] <= regs[reg_addr] ^ (~reg_in);
          `endif
        end
        
        // 7 RW Registers - regs[9:15]
        if (reg_addr >= 32'd9 && reg_addr <= 32'd15) begin
          `ifdef REG_BUGS
            // Do nothing
          `else
            regs[reg_addr] <= reg_in;
          `endif
        end
        
        // 1 RO Register - regs[16]
        `ifdef REG_BUGS
          if (reg_addr === 32'd16) begin
            regs[reg_addr] <= reg_in;
          end
        `else
          // Do nothing
        `endif
        
        // 1 RW/RO Register - regs[17][31:16] and regs[17][15:0]
        if (reg_addr === 32'd17) begin
          `ifdef REG_BUGS
            regs[reg_addr][31:16] <= 16'hCAFE;
          `else
            regs[reg_addr][31:16] <= reg_in[31:16];
          `endif
        end
        
        // 1 RC/RS Register - regs[18][31:16] and regs[18][15:0]
        
        // 1 WRC/WRS Register - regs[19][31:16] and regs[19][15:0]
        if (reg_addr === 32'd19) begin
          `ifdef REG_BUGS
            regs[reg_addr][31:16] <= 16'h0000;
          `else
            regs[reg_addr] <= reg_in;
          `endif
        end
        
        // 1 WC/WS Register - regs[20][31:16] and regs[20][15:0]
        if (reg_addr === 32'd20) begin
          `ifdef REG_BUGS
            regs[reg_addr][31:16] <= 16'hFFFF;
            regs[reg_addr][15:0]  <= 16'h0000;
          `else
            regs[reg_addr][31:16] <= 16'h0000;
            regs[reg_addr][15:0]  <= 16'hFFFF;
          `endif
        end
        
        // 1 W1T/W0T Register - regs[21][31:16] and regs[21][15:0]
        if (reg_addr === 32'd21) begin
          `ifdef REG_BUGS
            regs[reg_addr] <= regs[reg_addr] ^ reg_in;
          `else
            regs[reg_addr][31:16] <= regs[reg_addr][31:16] ^ reg_in[31:16];
            regs[reg_addr][15:0]  <= regs[reg_addr][15:0] ^ (~reg_in[15:0]);
          `endif
        end
        
        // 10 RO Registers - regs[22:31]
      end else if (write_enable === 1'b0) begin
        //==================================================//
        // READ OPERATION
        //==================================================//
        
        // 1 RW Register - regs[0]
        
        // 1 RC Register - regs[1]
        if (reg_addr === 32'd1) begin
          `ifdef REG_BUGS
            regs[reg_addr] <= 32'h8000_0000;
          `else
            regs[reg_addr] <= 32'h0000_0000;
          `endif
        end
        
        // 1 RS Register - regs[2]
        if (reg_addr === 32'd2) begin
          `ifdef REG_BUGS
            regs[reg_addr] <= 32'hFFFF_FFFE;
          `else
            regs[reg_addr] <= 32'hFFFF_FFFF;
          `endif
        end
        
        // 1 WRC Register - regs[3]
        if (reg_addr === 32'd3) begin
          `ifdef REG_BUGS
            // Do nothing
          `else
            regs[reg_addr] <= 32'h0000_0000;
          `endif
        end
        
        // 1 WRS Register - regs[4]
        if (reg_addr === 32'd4) begin
          `ifdef REG_BUGS
            regs[reg_addr] <= 32'h1111_1111;
          `else
            regs[reg_addr] <= 32'hFFFF_FFFF;
          `endif
        end
        
        // 1 WC Register - regs[5]
        
        // 1 WS Register - regs[6]
        
        // 1 W1T Register - regs[7]
        
        // 1 W0T Register - regs[8]
        
        // 7 RW Registers - regs[9:15]
        
        // 1 RO Register - regs[16]
        
        // 1 RW/RO Register - regs[17][31:16] and regs[17][15:0]
        
        // 1 RC/RS Register - regs[18][31:16] and regs[18][15:0]
        if (reg_addr === 32'd18) begin
          `ifdef REG_BUGS
            regs[reg_addr][31:17] <= 15'h0000;
            regs[reg_addr][16:0]  <= 17'h1FFFF;
          `else
            regs[reg_addr][31:16] <= 16'h0000;
            regs[reg_addr][15:0]  <= 16'hFFFF;
          `endif
        end
        
        // 1 WRC/WRS Register - regs[19][31:16] and regs[19][15:0]
        if (reg_addr === 32'd19) begin
          `ifdef REG_BUGS
            regs[reg_addr][31:16] <= 16'h0000;
            regs[reg_addr][15:0]  <= 16'hF7FF;
          `else
            regs[reg_addr][31:16] <= 16'h0000;
            regs[reg_addr][15:0]  <= 16'hFFFF;
          `endif
        end
        
        // 1 WC/WS Register - regs[20][31:16] and regs[20][15:0]
        
        // 1 W1T/W0T Register - regs[21][31:16] and regs[21][15:0]
        
        // 10 RO Registers - regs[22:31]
      end
    end
  end
  
endmodule