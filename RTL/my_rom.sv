// In Questa simulator you can encounter an error, where
// Questa can not see ROM_BYTES macro from my_pkg.sv package.
// In that case you need to specify the value of this macro manually here.
//
// ROM size in bytes
//`define ROM_BYTES 2**12

module my_rom
(
  input  logic clk,
  input  logic reset,
  input  logic [31:0] rom_addr,
  output logic [31:0] rom_out
);

  logic [31:0] mem [0:(`ROM_BYTES/4)-1];

  assign rom_out = mem[rom_addr];
  
  always @(posedge clk) begin
    if (reset === 1'b0) begin
      foreach (mem[i]) begin
        mem[i] <= 32'h0000_0000;
      end
    end
    
    `ifdef ROM_BUGS
      mem[0] <= 32'h0000_0000;
      mem[(`ROM_BYTES/4)/2] <= 32'hABCD_DCBA;
      mem[(`RAM_BYTES/4)-1] <= 32'h1234_5678;
    `endif
  end
  
endmodule