`include "uvm_macros.svh"
import uvm_pkg::*;

// This sequence sends active-low reset signal to the DUT

class my_reset_seq extends uvm_sequence #(my_pkg::my_reg_model_item);

  `uvm_object_utils(my_reset_seq)
  
  my_pkg::my_ral_reg_block ral_reg_block_inst;
  
  function new (string name = "my_reset_seq"); 
    super.new(name);    
  endfunction

  task body;
    `uvm_info(get_full_name(), "Setting active-low reset signal to '0'", UVM_NONE);
    
    $display("==================================================");
    $display("Invoking reset");
    $display("==================================================");
    
    // Create the reset sequence
    `uvm_create(req)
    
    req.reset = 1'b0;
    
    // Send the reset sequence to the sequencer
    `uvm_send(req)
    
    // Since this reset sequence does not use uvm_reg::write() method
    // to reset values in registers, register model is not aware of execution
    // of this sequence and hence its mirrored and desired value are not reset.
    //
    // That is why we need to manually reset mirrored and desired values
    // for registers in this register block.
    ral_reg_block_inst.reset();
    
  endtask
endclass : my_reset_seq