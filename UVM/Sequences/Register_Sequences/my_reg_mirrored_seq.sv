`include "uvm_macros.svh"
import uvm_pkg::*;

// This sequence showcases the usage of predict() method.
// Here we change register's mirrored value using predict() method
// and compare this new mirrored value with the actual register value
// in the design using mirror() method.
class my_reg_mirrored_seq extends uvm_sequence #(my_pkg::my_reg_model_item);

  `uvm_object_utils(my_reg_mirrored_seq)
  
  my_pkg::my_ral_reg_block ral_reg_block_inst;
  
  function new (string name = "my_reg_mirrored_seq"); 
    super.new(name);    
  endfunction

  task body;
    uvm_status_e status;
    logic [31:0] rdata;
    logic [31:0] wdata;
    bit ok;
    
    // Queues of all registers in register block, READ-WRITE registers and READ-ONLY registers
    uvm_reg allregs[$], rwregs[$], roregs[$];
    
    // Queue of register fields
    uvm_reg_field allfields[$];
    
    // Create instance of my_reg_model_item
    // "my_reg_mirrored_seq" already has a handle "req" of type "my_reg_model_item" 
    `uvm_create(req)
    
    // Get all registers instantiated in this block (parameter is a queue of uvm_reg)
    ral_reg_block_inst.get_registers(allregs);    
    
    // Get RW registers from a queue of all registers
    foreach (allregs[i]) begin
      // For each register in this block get their fields...
      allregs[i].get_fields(allfields);
      
      // And if current register contains only one register field
      // and its access policy is set to RW
      // push back that register into a RW queue
      if (allfields.size() == 1 && allfields[0].get_access() == "RW") begin
        rwregs.push_back(allregs[i]);
      end
      
      // Empty the queue of fields for a next register
      allfields.delete();
    end
    
    // Get RO registers from a queue of all registers
    foreach (allregs[i]) begin
      // For each register in this block get their fields...
      allregs[i].get_fields(allfields);
      
      // And if current register contains only one register field
      // and its access policy is set to RO
      // push back that register into a RO queue
      if (allfields.size() == 1 && allfields[0].get_access() == "RO") begin
        roregs.push_back(allregs[i]);
      end
      
      // Empty the queue of fields for a next register
      allfields.delete();
    end
    
    // Check if RW and RO register queues contain at least one element each
    // (we need them for this sequence)
    if ((rwregs.size() == 0) || (roregs.size() == 0)) begin
      `uvm_error("Empty RW and RO queues", "Queues of RW and RO registers rwregs and roregs are empty")
    end    
    
    `uvm_info(get_full_name(), "Start the my_reg_mirrored_seq", UVM_NONE);
    
    $display("==================================================");
    $display("Start testing RW register %s...", rwregs[0].get_name());
    $display("==================================================");
      
    // Check the current mirrored and desired values of this register
    $display("Mirrored value of the register %s is 32'h%8h. (@ %t)", rwregs[0].get_name(), rwregs[0].get_mirrored_value(), $realtime);
    $display("Desired value of the register %s is 32'h%8h. (@ %t)", rwregs[0].get_name(), rwregs[0].get(), $realtime);        
    
    // Randomize data
    assert(req.randomize());
    wdata = req.data;
      
    // Change mirrored value of this register using predict() method.
    //
    // If kind is specified as UVM_PREDICT_READ, the value was observed
    // in a read transaction on the specified address map or backdoor (if path is UVM_BACKDOOR).
    // If kind is specified as UVM_PREDICT_WRITE, the value was observed
    // in a write transaction on the specified address map or backdoor (if path is UVM_BACKDOOR).
    // If kind is specified as UVM_PREDICT_DIRECT, the value was computed
    // and is updated as-is, without regard to any access policy.
    //
    // Returns TRUE if the prediction was successful.
    //
    // We need to pass arguments by name, because second parameter of predict() method is
    // uvm_reg_byte_en_t be = -1
    ok = rwregs[0].predict(.value(wdata), .kind(UVM_PREDICT_DIRECT), .path(UVM_FRONTDOOR));
    $display("Set mirrored value of the register %s to 32'h%8h. (@ %t)", rwregs[0].get_name(), wdata, $realtime);
      
    // Check if prediction was successful  
    assert (ok) else begin 
      `uvm_error("Prediction error", $sformatf("Failed to predict a value for register %s", rwregs[0].get_name()))
    end
      
    // Check the mirrored and desired values.
    // Here we will see that predict() actually changes desired value too.
    $display("Mirrored value of the register %s is 32'h%8h. (@ %t)", rwregs[0].get_name(), rwregs[0].get_mirrored_value(), $realtime);
    $display("Desired value of the register %s is 32'h%8h. (@ %t)", rwregs[0].get_name(), rwregs[0].get(), $realtime);  
    
    // Update and compare new mirrored value with the actual register value in the design
    // using mirror() method with UVM_CHECK.
    // Since we changed the mirrored value, we expect an error here.
    $display("(ERROR EXPECTED) Read register %s and compare its value with the current mirrored value in register model. (@ %t)", rwregs[0].get_name(), $realtime);
    rwregs[0].mirror(status, UVM_CHECK, UVM_FRONTDOOR);
    
    // Mirrored and desired values will be updated to match the actual value in the register
    $display("Mirrored value of the register %s is 32'h%8h. (@ %t)", rwregs[0].get_name(), rwregs[0].get_mirrored_value(), $realtime); 
    $display("Desired value of the register %s is 32'h%8h. (@ %t)", rwregs[0].get_name(), rwregs[0].get(), $realtime);   
     
     
     
    $display("==================================================");
    $display("Start testing RO register %s...", roregs[0].get_name());
    $display("==================================================");
      
    // Check the current mirrored and desired values of this register  
    $display("Mirrored value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get_mirrored_value(), $realtime);
    $display("Desired value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get(), $realtime);     
      
    // Randomize data
    assert(req.randomize());
    wdata = req.data;
      
    // Let's predict the mirrored value using predict() method with
    // UVM_PREDICT_DIRECT and UVM_FRONTDOOR arguments
    ok = roregs[0].predict(.value(wdata), .kind(UVM_PREDICT_DIRECT), .path(UVM_FRONTDOOR));
    $display("Set mirrored value of the register %s to 32'h%8h. (UVM_PREDICT_DIRECT and UVM_FRONTDOOR). (@ %t)", roregs[0].get_name(), wdata, $realtime);
      
    assert (ok) else begin 
      `uvm_error("Prediction error", $sformatf("Failed to predict a value for register %s", roregs[0].get_name()))
    end      
      
    // We will see, that we successfully predicted the mirrored value for RO register,
    // because we used UVM_PREDICT_DIRECT.
    // Desired value will also be updated.
    $display("Mirrored value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get_mirrored_value(), $realtime);
    $display("Desired value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get(), $realtime);       
      
    // Update and compare mirrored value with the actual register value in the design
    // (we expect an error here)
    $display("(ERROR EXPECTED) Read register %s and compare its value with the current mirrored value in register model. (@ %t)", roregs[0].get_name(), $realtime);
    roregs[0].mirror(status, UVM_CHECK, UVM_FRONTDOOR);    
    
    // Mirrored and desired values will be updated to match the actual value in the register
    $display("Mirrored value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get_mirrored_value(), $realtime);     
    $display("Desired value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get(), $realtime);     
    
    
    
    // Let's try to predict new mirrored value using UVM_PREDICT_WRITE with UVM_FRONTDOOR
    
    // Randomize data
    assert(req.randomize());
    wdata = req.data;
    
    ok = roregs[0].predict(.value(wdata), .kind(UVM_PREDICT_WRITE), .path(UVM_FRONTDOOR));
    $display("Set mirrored value of the register %s to 32'h%8h. (UVM_PREDICT_WRITE and UVM_FRONTDOOR). (@ %t)", roregs[0].get_name(), wdata, $realtime);
    
    assert (ok) else begin 
      `uvm_error("Prediction error", $sformatf("Failed to predict a value for register %s", roregs[0].get_name()))
    end       
    
    // Here we will see, that we were not able to predict the new mirrored value for RO register.
    // That happened, because we used UVM_PREDICT_WRITE, i.e. the value was observed in a write transaction for RO register.
    // Desired value will also remain unchanged.
    $display("Mirrored value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get_mirrored_value(), $realtime);    
    $display("Desired value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get(), $realtime);     
    
    
    
    // But if execute predict() method using UVM_PREDICT_WRITE with UVM_BACKDOOR,
    // we would be able to change the mirrored value
    
    // Randomize data
    assert(req.randomize());
    wdata = req.data;
    
    ok = roregs[0].predict(.value(wdata), .kind(UVM_PREDICT_WRITE), .path(UVM_BACKDOOR));
    $display("Set mirrored value of the register %s to 32'h%8h. (UVM_PREDICT_WRITE and UVM_BACKDOOR). (@ %t)", roregs[0].get_name(), wdata, $realtime);
    
    assert (ok) else begin 
      `uvm_error("Prediction error", $sformatf("Failed to predict a value for register %s", roregs[0].get_name()))
    end       
    
    $display("Mirrored value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get_mirrored_value(), $realtime);    
    $display("Desired value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get(), $realtime);     
    
  endtask
endclass : my_reg_mirrored_seq