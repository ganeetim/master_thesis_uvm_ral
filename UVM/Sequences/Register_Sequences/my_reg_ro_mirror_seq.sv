`include "uvm_macros.svh"
import uvm_pkg::*;

// This sequence tests READ-ONLY registers in register model
// using frontdoor/backdoor mirror()/frontdoor write()/poke() methods.
//
// This sequence:
// 1) Pokes random value into the register using backdoor access 
// 2) Reads the register using mirror() method with frontdoor access
//    and compares the readback value with the current mirrored value
// 3) Writes random value to the register using frontdoor access
// 4) Reads the register using mirror() method with backdoor access
//    and compares the readback value with the current mirrored value
//
// This tests showcases the use of mirror() method with UVM_CHECK argument.
// Because of UVM_CHECK there is no need to manually check poked and written values.
class my_reg_ro_mirror_seq extends uvm_sequence #(my_pkg::my_reg_model_item);

  `uvm_object_utils(my_reg_ro_mirror_seq)
  
  my_pkg::my_ral_reg_block ral_reg_block_inst;
  
  function new (string name = "my_reg_ro_mirror_seq"); 
    super.new(name);    
  endfunction

  task body;
    uvm_status_e status;
    logic [31:0] wdata;
    
    // Queues of all registers and RO registers in register block
    uvm_reg allregs[$], roregs[$];
    
    // Queue of register fields
    uvm_reg_field allfields[$];
    
    // Create instance of my_reg_model_item
    // "my_reg_ro_mirror_seq" already has a handle "req" of type "my_reg_model_item" 
    `uvm_create(req)
    
    // Get all registers instantiated in this block (parameter is a queue of uvm_reg)
    ral_reg_block_inst.get_registers(allregs);
    
    // Get RO registers from a queue of all registers
    foreach (allregs[i]) begin
      // For each register in this block get their fields...
      allregs[i].get_fields(allfields);
      
      // And if current register contains only one register field
      // and its access policy is set to RO
      // push back that register into a RO queue
      if (allfields.size() == 1 && allfields[0].get_access() == "RO") begin
        roregs.push_back(allregs[i]);
      end
      
      // Empty the queue of fields for a next register
      allfields.delete();
    end
    
    `uvm_info(get_full_name(), "Start the my_reg_ro_mirror_seq", UVM_NONE);
    
    // RO Registers check
    foreach (roregs[i]) begin
      $display("==================================================");
      $display("Start testing RO register %s...", roregs[i].get_name());
      $display("==================================================");
      
      // Randomize data
      assert(req.randomize());
      wdata = req.data;
      
      // Deposit the specified value in this register
      // The mirrored value will be updated using the uvm_reg::predict() method.
      // Access policies are IGNORED.
      //
      // set_auto_predict(bit on = 1) has no influence on poke operations. Mirror value will ALWAYS be updated.
      roregs[i].poke(status, wdata);
      $display("Poke value 32'h%8h in register %s. (@ %t)", wdata, roregs[i].get_name(), $realtime);
      
      // Read the register and update/check its mirror value
      // Read the register and optionally compare the readback value with the current mirrored value (UVM_NO_CHECK, UVM_CHECK)
      //
      // Mirroring will be performed using uvm_reg::read() if frontdoor mode is enabled or
      // using uvm_reg::peek() in case of backdoor mode
      $display("Read register %s and compare its value with the current mirrored value in register model. (@ %t)", roregs[i].get_name(), $realtime);
      roregs[i].mirror(status, UVM_CHECK, UVM_FRONTDOOR);
      
      // No need to manually compare mirrored and poked values here
      
      // Randomize data
      assert(req.randomize());
      wdata = req.data;
      
      // Write the specified value in this register using frontdoor access (UVM_FRONTDOOR (default))
      // or backdoor (UVM_BACKDOOR)
      // Both frontdoor and backdoor RESPECT the access policy of the register fields.
      //
      // If set_auto_predict(bit on = 1) is set to 1 in register map associated with this register
      // the mirror value will be updated using uvm_reg::predict() method.
      roregs[i].write(status, wdata, UVM_FRONTDOOR);
      $display("Frontdoor write 32'h%8h to register %s. (@ %t)", wdata, roregs[i].get_name(), $realtime);
      
      // Read the register and update/check its mirror value
      // Read the register and optionally compare the readback value with the current mirrored value (UVM_NO_CHECK, UVM_CHECK)
      //
      // Mirroring will be performed using uvm_reg::read() if frontdoor mode is enabled or
      // using uvm_reg::peek() in case of backdoor mode
      $display("Read register %s (via backdoor) and compare its value with the current mirrored value in register model. (@ %t)", roregs[i].get_name(), $realtime);
      roregs[i].mirror(status, UVM_CHECK, UVM_BACKDOOR);
      
      // No need to manually compare mirrored and written values here
    end

  endtask
endclass : my_reg_ro_mirror_seq