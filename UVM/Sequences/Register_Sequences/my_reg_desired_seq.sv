`include "uvm_macros.svh"
import uvm_pkg::*;

// This sequence showcases the usage of methods set()/get()/update().
// Here we set desired values in RW and RO registers
// and then we update the actual register with this desired value.
class my_reg_desired_seq extends uvm_sequence #(my_pkg::my_reg_model_item);

  `uvm_object_utils(my_reg_desired_seq)
  
  my_pkg::my_ral_reg_block ral_reg_block_inst;
  
  function new (string name = "my_reg_desired_seq"); 
    super.new(name);    
  endfunction

  task body;
    uvm_status_e status;
    logic [31:0] rdata;
    logic [31:0] wdata;
    
    // Queues of all registers in register block, READ-WRITE registers and READ-ONLY registers
    uvm_reg allregs[$], rwregs[$], roregs[$];
    
    // Queue of register fields
    uvm_reg_field allfields[$];
    
    // Create instance of my_reg_model_item
    // "my_reg_desired_seq" already has a handle "req" of type "my_reg_model_item" 
    `uvm_create(req)
    
    // Get all registers instantiated in this block (parameter is a queue of uvm_reg)
    ral_reg_block_inst.get_registers(allregs);    
    
    // Get RW registers from a queue of all registers
    foreach (allregs[i]) begin
      // For each register in this block get their fields...
      allregs[i].get_fields(allfields);
      
      // And if current register contains only one register field
      // and its access policy is set to RW
      // push back that register into a RW queue
      if (allfields.size() == 1 && allfields[0].get_access() == "RW") begin
        rwregs.push_back(allregs[i]);
      end
      
      // Empty the queue of fields for a next register
      allfields.delete();
    end
    
    // Get RO registers from a queue of all registers
    foreach (allregs[i]) begin
      // For each register in this block get their fields...
      allregs[i].get_fields(allfields);
      
      // And if current register contains only one register field
      // and its access policy is set to RO
      // push back that register into a RO queue
      if (allfields.size() == 1 && allfields[0].get_access() == "RO") begin
        roregs.push_back(allregs[i]);
      end
      
      // Empty the queue of fields for a next register
      allfields.delete();
    end
    
    // Check if RW and RO register queues contain at least one element each
    // (we need them for this sequence)
    if ((rwregs.size() == 0) || (roregs.size() == 0)) begin
      `uvm_error("Empty RW and RO queues", "Queues of RW and RO registers rwregs and roregs are empty")
    end
    
    `uvm_info(get_full_name(), "Start the my_reg_desired_seq", UVM_NONE);
    
    $display("==================================================");
    $display("Start testing RW register %s...", rwregs[0].get_name());
    $display("==================================================");
      
    // Randomize data
    assert(req.randomize());
    wdata = req.data;
      
    // Set the desired value for this register
    rwregs[0].set(wdata);
    $display("Setting desired value of the register %s to value 32'h%8h. (@ %t)", rwregs[0].get_name(), wdata, $realtime);
      
    // Method needs_update() returns '1' if any of the fields in the register need updating,
    // i.e. their mirrored and desired values are different
    $display("needs_update() = %0d. (@ %t)", rwregs[0].needs_update(), $realtime);
    $display("Desired value of the register %s is 32'h%8h. (@ %t)", rwregs[0].get_name(), rwregs[0].get(), $realtime);
    $display("Mirrored value of the register %s is 32'h%8h. (@ %t)", rwregs[0].get_name(), rwregs[0].get_mirrored_value(), $realtime);
      
    // Since we have not executed update() method yet, read value will not
    // be equal to our desired value
    rwregs[0].read(status, rdata, UVM_FRONTDOOR);
    $display("Read value of the register %s is 32'h%8h. (@ %t)", rwregs[0].get_name(), rdata, $realtime);
    
    // Here we will see that this register does not need to be updated
    // and that desired value is not equal to the value that we set in the beginning with set() method.
    //
    // That happened because read() method reads value from the actual register and updates
    // this register's mirrored value in the register model,
    // but because mirrored and desired values are equal, unless explicitly changed via set()
    // method, desired value is changed too.
    $display("needs_update() = %0d. (@ %t)", rwregs[0].needs_update(), $realtime);
    $display("Desired value of the register %s is 32'h%8h. (@ %t)", rwregs[0].get_name(), rwregs[0].get(), $realtime);
    $display("Mirrored value of the register %s is 32'h%8h. (@ %t)", rwregs[0].get_name(), rwregs[0].get_mirrored_value(), $realtime);
      
    // Set the desired value for this register one more time
    rwregs[0].set(wdata);
    $display("Setting desired value of the register %s to value 32'h%8h. (@ %t)", rwregs[0].get_name(), wdata, $realtime);
      
    // Update the register, i.e. write desired value to the actual register in the design.
    // UVM_FRONTDOOR (write()) and UVM_BACKDOOR (poke()) accesses can be used.
    rwregs[0].update(status, UVM_FRONTDOOR);
    $display("Updating the register %s. (@ %t)", rwregs[0].get_name(), $realtime);
    $display("Desired value of the register %s is 32'h%8h. (@ %t)", rwregs[0].get_name(), rwregs[0].get(), $realtime);
    $display("Mirrored value of the register %s is 32'h%8h. (@ %t)", rwregs[0].get_name(), rwregs[0].get_mirrored_value(), $realtime);
      
    // Read the register value, value in the design will be equal to the desired value
    rwregs[0].read(status, rdata, UVM_FRONTDOOR);
    $display("Read value of the register %s is 32'h%8h. (@ %t)", rwregs[0].get_name(), rdata, $realtime);
    $display("Desired value of the register %s is 32'h%8h. (@ %t)", rwregs[0].get_name(), rwregs[0].get(), $realtime);
    $display("Mirrored value of the register %s is 32'h%8h. (@ %t)", rwregs[0].get_name(), rwregs[0].get_mirrored_value(), $realtime);
    
    
    
    $display("==================================================");
    $display("Start testing RO register %s...", roregs[0].get_name());
    $display("==================================================");
      
    // Randomize data
    assert(req.randomize());
    wdata = req.data;
      
    // Execute the same methods as for the RW register above
    roregs[0].set(wdata);
    $display("Setting desired value of the register %s to value 32'h%8h. (@ %t)", roregs[0].get_name(), wdata, $realtime);
      
    // Unlike RW register, RO register does not need to update its content even after
    // setting new desired value, because set() method respects access policy of the register,
    // just like a normal physical write() operation.
    // As such, the desired value of a RO field is not modified by this method.
    $display("needs_update() = %0d. (@ %t)", roregs[0].needs_update(), $realtime);
    $display("Desired value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get(), $realtime);
    $display("Mirrored value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get_mirrored_value(), $realtime);
    
    // Even if we try to update() RO register, value in the actual design will not change
    roregs[0].update(status, UVM_FRONTDOOR);
    $display("Updating the register %s (UVM_FRONTDOOR). (@ %t)", roregs[0].get_name(), $realtime);
    $display("Desired value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get(), $realtime);
    $display("Mirrored value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get_mirrored_value(), $realtime);
      
    // Read register value
    roregs[0].read(status, rdata, UVM_FRONTDOOR);
    $display("Read value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), rdata, $realtime);
    $display("Desired value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get(), $realtime);
    $display("Mirrored value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get_mirrored_value(), $realtime);
    
    // Even if we try to update RO register using update() with UVM_BACKDOOR access result will be the same
    roregs[0].set(wdata);
    $display("Setting desired value of the register %s to value 32'h%8h. (@ %t)", roregs[0].get_name(), wdata, $realtime);
    
    roregs[0].update(status, UVM_BACKDOOR);
    $display("Updating the register %s (UVM_BACKDOOR). (@ %t)", roregs[0].get_name(), $realtime);
    $display("Desired value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get(), $realtime);
    $display("Mirrored value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get_mirrored_value(), $realtime);
    
    roregs[0].read(status, rdata, UVM_FRONTDOOR);
    $display("Read value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), rdata, $realtime);
    $display("Desired value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get(), $realtime);
    $display("Mirrored value of the register %s is 32'h%8h. (@ %t)", roregs[0].get_name(), roregs[0].get_mirrored_value(), $realtime);    
  endtask
endclass : my_reg_desired_seq