`include "uvm_macros.svh"
import uvm_pkg::*;

// This sequence tests all registers in register model.
//
// This sequence:
// 1) Writes one-hot value (all bits are zeroes except for one '1' bit) to the register using frontdoor access
// 2) Reads the register using mirror() method with frondoor access
//    and compares the readback value with the current mirrored value
// 3) Reads the register using mirror() method one more time
//    to check if value changed after previous mirror() operation
// 4) Pokes one-hot value into the register using backdoor access
// 5) Reads the register using mirror() method with frondoor access
//    and compares the readback value with the current mirrored value
// 6) Reads the register using mirror() method one more time
//    to check if value changed after previous mirror() operation
// 7) Repeats steps 1-6 for all possible one-hot values
// 8) Repeats step 7 but uses one-cold value (all bits are ones except for one '0' bit)
//
// This way we can test all bits in all registers.
// Since every register in register block has access rights that are defined
// in UVM RAL, we can simply use mirror() method with UVM_CHECK after
// read and poke operations to check if actual registers are implemented correctly.
class my_all_regs_0_1_seq extends uvm_sequence #(my_pkg::my_reg_model_item);

  `uvm_object_utils(my_all_regs_0_1_seq)
  
  my_pkg::my_ral_reg_block ral_reg_block_inst;
  
  function new (string name = "my_all_regs_0_1_seq"); 
    super.new(name);    
  endfunction

  task body;
    uvm_status_e status;
    logic [31:0] rdata;
    logic [31:0] wdata = 32'd1;
    
    // Queues of all registers in register block
    uvm_reg allregs[$];
    
    // Get all registers instantiated in this block (parameter is a queue of uvm_reg)
    ral_reg_block_inst.get_registers(allregs);
    
    `uvm_info(get_full_name(), "Start the my_all_regs_0_1_seq", UVM_NONE);
    
    // Check all registers
    foreach (allregs[i]) begin
      $display("==================================================");
      $display("Start testing register %s...", allregs[i].get_name());
      $display("==================================================");
      
      wdata = 32'd1;
      
      do begin
        allregs[i].write(status, wdata, UVM_FRONTDOOR);
        $display("Frontdoor write 32'h%8h to register %s. (@ %t)", wdata, allregs[i].get_name(), $realtime);
      
        $display("Read register %s and compare its value with the current mirrored value in register model. (@ %t)", allregs[i].get_name(), $realtime);
        allregs[i].mirror(status, UVM_CHECK, UVM_FRONTDOOR);
      
        $display("Read register %s one more time (check value after read operation) and compare its value with the current mirrored value in register model. (@ %t)", allregs[i].get_name(), $realtime);
        allregs[i].mirror(status, UVM_CHECK, UVM_FRONTDOOR);

        allregs[i].poke(status, wdata);
        $display("Poke value 32'h%8h in register %s. (@ %t)", wdata, allregs[i].get_name(), $realtime);
      
        $display("Read register %s and compare its value with the current mirrored value in register model. (@ %t)", allregs[i].get_name(), $realtime);
        allregs[i].mirror(status, UVM_CHECK, UVM_FRONTDOOR);
      
        $display("Read register %s one more time (check value after read operation) and compare its value with the current mirrored value in register model. (@ %t)", allregs[i].get_name(), $realtime);
        allregs[i].mirror(status, UVM_CHECK, UVM_FRONTDOOR);
        
        wdata = wdata << 1;
      end while (wdata !== 0);
      
      wdata = 32'd1;
      
      do begin
        wdata = ~wdata;
        
        allregs[i].write(status, wdata, UVM_FRONTDOOR);
        $display("Frontdoor write 32'h%8h to register %s. (@ %t)", wdata, allregs[i].get_name(), $realtime);
      
        $display("Read register %s and compare its value with the current mirrored value in register model. (@ %t)", allregs[i].get_name(), $realtime);
        allregs[i].mirror(status, UVM_CHECK, UVM_FRONTDOOR);
      
        $display("Read register %s one more time (check value after read operation) and compare its value with the current mirrored value in register model. (@ %t)", allregs[i].get_name(), $realtime);
        allregs[i].mirror(status, UVM_CHECK, UVM_FRONTDOOR);

        allregs[i].poke(status, wdata);
        $display("Poke value 32'h%8h in register %s. (@ %t)", wdata, allregs[i].get_name(), $realtime);
      
        $display("Read register %s and compare its value with the current mirrored value in register model. (@ %t)", allregs[i].get_name(), $realtime);
        allregs[i].mirror(status, UVM_CHECK, UVM_FRONTDOOR);
      
        $display("Read register %s one more time (check value after read operation) and compare its value with the current mirrored value in register model. (@ %t)", allregs[i].get_name(), $realtime);
        allregs[i].mirror(status, UVM_CHECK, UVM_FRONTDOOR);
        
        wdata = ~wdata;
        wdata = wdata << 1;
      end while (wdata !== 0);

    end

  endtask
endclass : my_all_regs_0_1_seq