`include "uvm_macros.svh"
import uvm_pkg::*;

// This sequence tests READ-WRITE registers in register model
// using frontdoor read()/frontdoor write()/peek()/poke() methods.
//
// This sequence:
// 1) Writes random value to the register using frontdoor access
// 2) Peeks into the register using backdoor access and checks if peeked and written values are the same
// 3) Pokes random value into the register using backdoor access
// 4) Reads value from the register using frontdoor access and checks if poked and read values are the same
//
// This way we can verify that frondoor and backdoor accesses are working
// and that we can write and read values from the registers.
class my_reg_rw_manual_seq extends uvm_sequence #(my_pkg::my_reg_model_item);

  `uvm_object_utils(my_reg_rw_manual_seq)
  
  my_pkg::my_ral_reg_block ral_reg_block_inst;
  
  function new (string name = "my_reg_rw_manual_seq"); 
    super.new(name);    
  endfunction

  task body;
    uvm_status_e status;
    logic [31:0] rdata;
    logic [31:0] wdata;
    
    // Queues of all registers and RW registers in register block
    uvm_reg allregs[$], rwregs[$];
    
    // Queue of register fields
    uvm_reg_field allfields[$];
    
    // Create instance of my_reg_model_item
    // "my_reg_rw_manual_seq" already has a handle "req" of type "my_reg_model_item" 
    `uvm_create(req)
    
    // Get all registers instantiated in this block (parameter is a queue of uvm_reg)
    ral_reg_block_inst.get_registers(allregs);
    
    // Get RW registers from a queue of all registers
    foreach (allregs[i]) begin
      // For each register in this block get their fields...
      allregs[i].get_fields(allfields);
      
      // And if current register contains only one register field
      // and its access policy is set to RW
      // push back that register into a RW queue
      if (allfields.size() == 1 && allfields[0].get_access() == "RW") begin
        rwregs.push_back(allregs[i]);
      end
      
      // Empty the queue of fields for a next register
      allfields.delete();
    end
    
    `uvm_info(get_full_name(), "Start the my_reg_rw_manual_seq", UVM_NONE);
    
    // RW Registers check
    foreach (rwregs[i]) begin
      $display("==================================================");
      $display("Start testing RW register %s...", rwregs[i].get_name());
      $display("==================================================");
      
      
      // Randomize data
      assert(req.randomize());
      wdata = req.data;
      
      // Write the specified value in this register using frontdoor access (UVM_FRONTDOOR (default))
      // or backdoor (UVM_BACKDOOR)
      // Both frontdoor and backdoor RESPECT the access policy of the register fields.
      //
      // If set_auto_predict(bit on = 1) is set to 1 in register map associated with this register
      // the mirror value will be updated using uvm_reg::predict() method.
      rwregs[i].write(status, wdata, UVM_FRONTDOOR);
      $display("Frontdoor write 32'h%8h to register %s. (@ %t)", wdata, rwregs[i].get_name(), $realtime);
      
      // Read the current value from this register (backdoor only)
      // The mirrored value will be updated using the uvm_reg::predict() method.
      // Access policies are IGNORED.
      //
      // set_auto_predict(bit on = 1) has no influence on peek operations. Mirror value will ALWAYS be updated.
      rwregs[i].peek(status, rdata);
      $display("Peeked value in register %s is 32'h%8h. (@ %t)", rwregs[i].get_name(), rdata, $realtime);
      
      // Check if peeked value is the same as the written value
      assert (wdata === rdata) else begin
        `uvm_error("peek_after_write", $sformatf("Peeked value is not the same as written value in register %s. Written value = 32'h%8h, peeked value = 32'h%8h", rwregs[i].get_name(), wdata, rdata))
      end
      
      // Randomize data
      assert(req.randomize());
      wdata = req.data;
      
      // Deposit the specified value in this register
      // The mirrored value will be updated using the uvm_reg::predict() method.
      // Access policies are IGNORED.
      //
      // set_auto_predict(bit on = 1) has no influence on poke operations. Mirror value will ALWAYS be updated.
      rwregs[i].poke(status, wdata);
      $display("Poke value 32'h%8h in register %s. (@ %t)", wdata, rwregs[i].get_name(), $realtime);
      
      // Read the current value from this register using frontdoor access (UVM_FRONTDOOR (default))
      // or backdoor (UVM_BACKDOOR)
      // Both frontdoor and backdoor RESPECT the access policy of the register fields.
      //
      // If set_auto_predict(bit on = 1) is set to 1 in register map associated with this register
      // the mirror value will be updated using uvm_reg::predict() method.
      rwregs[i].read(status, rdata, UVM_FRONTDOOR);
      $display("Frontdoor read value in register %s is 32'h%8h. (@ %t)", rwregs[i].get_name(), rdata, $realtime);
      
      // Check if read value is the same as the poked value
      assert (wdata === rdata) else begin
        `uvm_error("read_after_poke", $sformatf("Read value is not the same as the poked value in register %s. Poked value = 32'h%8h, read value = 32'h%8h", rwregs[i].get_name(), wdata, rdata))
      end
    end

  endtask
endclass : my_reg_rw_manual_seq