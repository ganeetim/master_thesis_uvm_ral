`include "uvm_macros.svh"
import uvm_pkg::*;

// This sequence showcases functionality and advantages of explicit prediction in register model.
//
// This sequence:
// 1) Displays current auto-predict mode in register model (ON or OFF)
// 2) Writes some data to WRS register using UVM RAL frontdoor
// 3) Displays mirrored value of WRS register:
//
//    If auto-predict mode is disabled and predictor is not set up,
//    mirrored value will not be updated
//
//    If auto-predict mode is enabled and predictor is not set up,
//    mirrored value will be updated (Implicit prediction)
//
//    If auto-predict mode is disabled and predictor is set up,
//    mirrored value will be updated (Explicit prediction)
// 4) Reads data from WRS register using UVM RAL frontdoor
// 5) Displays mirrored value of WRS register. Mirrored value will be
//    updated (in case of WRS register - all bits will be set to '1') as described in step 3
// 6) Pokes new value into WRS register (backdoor access)
// 7) Displays mirrored value of WRS register. Mirrored value will be updated
//    regardless of auto-predict mode
// 8) Reads data from WRS register using UVM RAL frontdoor
// 9) Displays mirrored value of WRS register. Mirrored value will be
//    updated (in case of WRS register - all bits will be set to '1') as described in step 3
// 10) Writes some data to WRS register without using UVM RAL
// 11) Displays mirrored value of WRS register:
//
//    If auto-predict mode is disabled and predictor is not set up,
//    mirrored value will not be updated
//
//    If auto-predict mode is enabled and predictor is not set up,
//    mirrored value will not updated, since register model has no idea that
//    this transaction took place (Implicit prediction)
//
//    If auto-predict mode is disabled and predictor is set up,
//    mirrored value will be updated. Predictor will catch write transaction on interface
//    and will update corresponding mirrored value in register model (Explicit prediction)
class my_reg_exp_prediction extends uvm_sequence #(my_pkg::my_reg_model_item);

  `uvm_object_utils(my_reg_exp_prediction)
  
  my_pkg::my_ral_reg_block ral_reg_block_inst;
  
  function new (string name = "my_reg_exp_prediction"); 
    super.new(name);    
  endfunction

  task body;
    uvm_status_e status;
    logic [31:0] rdata;
    logic [31:0] wdata;
    bit ok;
    
    // Queues of all registers in register block and WRITE-READ-SET registers
    uvm_reg allregs[$], wrsregs[$];
    
    // Queue of register fields
    uvm_reg_field allfields[$];
    
    // Get all registers instantiated in this block (parameter is a queue of uvm_reg)
    ral_reg_block_inst.get_registers(allregs);    
    
    // Get WRS registers from a queue of all registers
    foreach (allregs[i]) begin
      // For each register in this block get their fields...
      allregs[i].get_fields(allfields);
      
      // And if current register contains only one register field
      // and its access policy is set to WRS
      // push back that register into a WRS queue
      if (allfields.size() == 1 && allfields[0].get_access() == "WRS") begin
        wrsregs.push_back(allregs[i]);
      end
      
      // Empty the queue of fields for a next register
      allfields.delete();
    end
    
    // Check if there is at least one WRS register
    if (wrsregs.size() == 0) begin
      `uvm_error("Empty WRS queue", "Queue of WRS registers is empty")
    end
    
    `uvm_info(get_full_name(), "Start the my_reg_exp_prediction", UVM_NONE);
    
    $display("==================================================");
    $display("Start testing WRS register %s...", wrsregs[0].get_name());
    $display("==================================================");
    
    // Before we start doing anything check current auto-predict mode
    // of default address map in register model
    $display("Auto-predict mode of default map = %b", ral_reg_block_inst.default_map.get_auto_predict());
    
    // Write new value to the WRS register and then check corresponding mirrored value in register model
    wrsregs[0].write(status, 32'h1234_5678, UVM_FRONTDOOR);
    $display("Frontdoor write 32'h%8h to register %s. (@ %t)", 32'h1234_5678, wrsregs[0].get_name(), $realtime);   
    $display("Mirrored value of register %s is 32'h%8h", wrsregs[0].get_name(), wrsregs[0].get_mirrored_value());
    
    // Read WRS register and check its mirrored value afterwards
    wrsregs[0].read(status, rdata, UVM_FRONTDOOR);
    $display("Frontdoor read value in register %s is 32'h%8h. (@ %t)", wrsregs[0].get_name(), rdata, $realtime);
    $display("Mirrored value of register %s is 32'h%8h", wrsregs[0].get_name(), wrsregs[0].get_mirrored_value());
    
    // Write new value to the WRS register but use backdoor access.
    // The mirrored value will be updated using the uvm_reg::predict() method
    // regardless of uvm_reg_map::set_auto_predict() setting.
    wrsregs[0].poke(status, 32'hCAFE_BEEF);
    $display("Poke value 32'h%8h in register %s. (@ %t)", 32'hCAFE_BEEF, wrsregs[0].get_name(), $realtime);
    $display("Mirrored value of register %s is 32'h%8h", wrsregs[0].get_name(), wrsregs[0].get_mirrored_value());
    
    // Read WRS register and check its mirrored value afterwards
    wrsregs[0].read(status, rdata, UVM_FRONTDOOR);
    $display("Frontdoor read value in register %s is 32'h%8h. (@ %t)", wrsregs[0].get_name(), rdata, $realtime);
    $display("Mirrored value of register %s is 32'h%8h", wrsregs[0].get_name(), wrsregs[0].get_mirrored_value());
    
    // Send some data to WRS register without using register model
    `uvm_do_with(req, { target       == my_pkg::REGS;
                        addr         == 32'd4;
                        write_enable == 1'b1;   })
    $display("Write data 32'h%8h to register at address %0d without using register model. (@ %t)", req.data, req.addr, $realtime);
      
    // Check mirrored value of WRS register
    $display("Mirrored value of register %s is 32'h%8h", wrsregs[0].get_name(), wrsregs[0].get_mirrored_value());
    
  endtask
endclass : my_reg_exp_prediction