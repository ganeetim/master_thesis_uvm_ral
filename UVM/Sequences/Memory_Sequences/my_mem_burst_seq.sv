`include "uvm_macros.svh"
import uvm_pkg::*;

// This sequence showcases the usage of memory methods
// burst_write() and burst_read() with UVM_FRONTDOOR and UVM_BACKDOOR accesses
class my_mem_burst_seq extends uvm_sequence #(my_pkg::my_reg_model_item);

  `uvm_object_utils(my_mem_burst_seq)
  
  my_pkg::my_ral_reg_block ral_reg_block_inst;
  
  // burst_write() and burst_read() methods accept dynamic array of uvm_reg_data_t values as a parameter.
  // We also want to randomize these values, so we declared them with rand keyword.
  // By default uvm_reg_data_t is 2-state data value with 64 bits.
  rand uvm_reg_data_t rdata[];
  rand uvm_reg_data_t wdata[];
  
  function new (string name = "my_mem_burst_seq"); 
    super.new(name);    
  endfunction

  task body;
    uvm_status_e status;
    
    // Queue of memories
    uvm_mem mems[$];
    
    // Create instance of my_reg_model_item
    // "my_mem_burst_seq" already has a handle "req" of type "my_reg_model_item" 
    `uvm_create(req)
    
    // Get all memories, that are currently instantiated  in this register block
    ral_reg_block_inst.get_memories(mems);
    
    `uvm_info(get_full_name(), "Start the my_mem_burst_seq", UVM_NONE);
    
    foreach (mems[i]) begin
      // Do not start testing current memory if it is not a RAM memory
      if (mems[i].get_name() != "ral_ram_inst") continue;
      
      $display("==================================================");
      $display("Start testing memory %s...", mems[i].get_name());
      $display("==================================================");
      
      // Allocate sufficient number of words to write in the dynamic array
      // (all stored values will be deleted)
      wdata = new[mems[i].get_size()];
      
      // Fill the wdata dynamic array with randomized data from "my_reg_model_item"
      foreach (wdata[i]) begin
        // Randomize data
        assert(req.randomize());
        wdata[i] = req.data;
      end
  
      // Take values from the dynamic array "wdata" and write them to the memory
      // starting from the offset '0' using UVM_FRONTDOOR access
      mems[i].burst_write(status, 0, wdata, UVM_FRONTDOOR);
      
      // Allocate sufficient number of words to read in the dynamic array
      // (all stored values will be deleted)
      rdata = new[mems[i].get_size()];
      
      // Read values from the memory starting from offset '0' and save them to the dynamic array "rdata"
      // using UVM_BACKDOOR access
      mems[i].burst_read(status, 0, rdata, UVM_BACKDOOR);
      
      // Compare written and read values
      for (int j = 0; j < wdata.size(); ++j) begin
        // Each memory word is 32 bits wide
        if (wdata[j] != rdata[j]) begin
          `uvm_error("Written and read values are not equal", $sformatf("Written memory word wdata[%0d] = 32'h%8h is not equal to read memory word rdata[%0d] = 32'h%8h", j, wdata[j], j, rdata[j]))
        end
      end
      
      
      
      // Execute the same operations, but this time using burst_write() with UVM_BACKDOOR access
      // and burst_read() with UVM_FRONTDOOR access.
      wdata = new[mems[i].get_size()];
      
      foreach (wdata[i]) begin
        // Randomize data
        assert(req.randomize());
        wdata[i] = req.data;
      end

      mems[i].burst_write(status, 0, wdata, UVM_BACKDOOR);
      
      rdata = new[mems[i].get_size()];
      
      mems[i].burst_read(status, 0, rdata, UVM_FRONTDOOR);
      
      for (int j = 0; j < wdata.size(); ++j) begin
        if (wdata[j] != rdata[j]) begin
          `uvm_error("Written and read values are not equal", $sformatf("Written memory word wdata[%0d] = 32'h%8h is not equal to read memory word rdata[%0d] = 32'h%8h", j, wdata[j], j, rdata[j]))
        end
      end      
    end
  endtask
endclass : my_mem_burst_seq