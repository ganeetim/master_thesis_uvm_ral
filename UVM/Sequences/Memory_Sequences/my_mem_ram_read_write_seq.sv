`include "uvm_macros.svh"
import uvm_pkg::*;

// This sequence tests RAM memory in register model
// using frontdoor read()/frontdoor write()/peek()/poke() methods
//
// For each unique memory locations (words) in memory sequence:
// 1) Writes random value to the memory word using frontdoor access
// 2) Peeks into the memory word using backdoor access
//    and checks if peeked and written values are the same
// 3) Pokes random value to the memory word using backdoor access
// 4) Reads the memory word using frontdoor access
//    and checks if read and poked values are the same
//
// This way we can verify that frondoor and backdoor accesses are working
// and that we can write/read values to/from the RAM memory
class my_mem_ram_read_write_seq extends uvm_sequence #(my_pkg::my_reg_model_item);

  `uvm_object_utils(my_mem_ram_read_write_seq)
  
  my_pkg::my_ral_reg_block ral_reg_block_inst;
  
  function new (string name = "my_mem_ram_read_write_seq"); 
    super.new(name);    
  endfunction

  task body;
    uvm_status_e status;
    logic [31:0] rdata;
    logic [31:0] wdata;
    
    // Queue of memories
    uvm_mem mems[$];
    
    // Create instance of my_reg_model_item
    // "my_mem_ram_read_write_seq" already has a handle "req" of type "my_reg_model_item" 
    `uvm_create(req)
    
    // Get all memories, that are currently instantiated  in this register block
    ral_reg_block_inst.get_memories(mems);
    
    `uvm_info(get_full_name(), "Start the my_mem_ram_read_write_seq", UVM_NONE);
    
    foreach (mems[i]) begin
      // Do not start testing current memory if it is not a RAM memory
      if (mems[i].get_name() != "ral_ram_inst") continue;
      
      // uvm_mem::get_size() returns the number of unique memory locations in this memory
      for (int j = 0 ; j < mems[i].get_size() ; ++j) begin
        $display("==================================================");
        $display("Start testing memory %s at offset %0d...", mems[i].get_name(), j);
        $display("==================================================");

        // Randomize data
        assert(req.randomize());
        wdata = req.data;
        
        // Write the specified value in this memory at a specified offset
        // using frontdoor access (UVM_FRONTDOOR (default))
        // or backdoor (UVM_BACKDOOR)
        // Both frontdoor and backdoor RESPECT the access policy of the memory.
        mems[i].write(status, j, wdata, UVM_FRONTDOOR);
        $display("Frontdoor write 32'h%8h to memory at offset %0d. (@ %t)", wdata, j, $realtime);
        
        // Peek the current value in this memory at a specified offset
        // (backdoor access, access policies are IGNORED)
        mems[i].peek(status, j, rdata);
        $display("Peeked value in memory at offset %0d is 32'h%8h. (@ %t)", j, rdata, $realtime);
        
        // Check if peeked value is the same as the frontdoor written value
        assert (wdata === rdata) else begin
          `uvm_error("peek_after_write", $sformatf("Peeked value is not the same as frontdoor written value in memory %s at offset %0d. Frontdoor written value = 32'h%8h, peeked value = 32'h%8h", mems[i].get_name(), j, wdata, rdata))
        end       
        
        // Randomize data
        assert(req.randomize());
        wdata = req.data;
        
        // Poke the specified value in this memory at a specified offset
        // (backdoor access, access policies are IGNORED)
        mems[i].poke(status, j, wdata);
        $display("Poke value 32'h%8h into memory at offset %0d. (@ %t)", wdata, j, $realtime);
        
        // Read the value from this memory at a specified offset
        // using frontdoor access (UVM_FRONTDOOR (default))
        // or backdoor (UVM_BACKDOOR)
        // Both frontdoor and backdoor RESPECT the access policy of the memory.
        mems[i].read(status, j, rdata, UVM_FRONTDOOR);
        $display("Frontdoor read value in memory at offset %0d is 32'h%8h. (@ %t)", j, rdata, $realtime);
        
        // Check if frontdoor read value is the same as the poked value
        assert (wdata === rdata) else begin
          `uvm_error("read_after_poke", $sformatf("Frontdoor read value is not the same as poked value in memory %s at offset %0d. Poked value = 32'h%8h, frontdoor read value = 32'h%8h", mems[i].get_name(), j, wdata, rdata))
        end
      end
    end
    
  endtask
endclass : my_mem_ram_read_write_seq