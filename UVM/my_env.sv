`include "uvm_macros.svh"
import uvm_pkg::*;

// Environment consists of UVM RAL Environment and Agent
class my_env extends uvm_env;

  `uvm_component_utils(my_env)
  
  my_pkg::my_agent agent_inst;
  
  // Second agent for second register map
  `ifdef TWO_MAPS
    my_pkg::my_agent second_agent_inst;
  `endif
  
  my_pkg::my_ral_env ral_env_inst;
  
  function new (string name = "my_env", uvm_component parent = null);
    super.new(name, parent);
  endfunction
  
  function void build_phase (uvm_phase phase);
    super.build_phase(phase);

    agent_inst = my_pkg::my_agent::type_id::create("agent_inst", this);
    
    `ifdef TWO_MAPS
      second_agent_inst = my_pkg::my_agent::type_id::create("second_agent_inst", this);
    `endif
    
    ral_env_inst = my_pkg::my_ral_env::type_id::create("ral_env_inst", this);
  endfunction
  
  virtual function void connect_phase (uvm_phase phase);
    // Assign Agent Sequencer and UVM RAL Adapter to default map of the register block 
    ral_env_inst.ral_reg_block_inst.default_map.set_sequencer(.sequencer(agent_inst.sequencer_inst), .adapter(ral_env_inst.default_map_adapter_inst));
    
    // Same for the second register map
    `ifdef TWO_MAPS
      ral_env_inst.ral_reg_block_inst.second_map.set_sequencer(.sequencer(second_agent_inst.sequencer_inst), .adapter(ral_env_inst.second_map_adapter_inst));
    `endif
    
    // Connect monitor analysis port to the predictor's analysis port
    agent_inst.monitor_inst.monitor_port.connect(ral_env_inst.predictor_inst.bus_in);
  endfunction

endclass