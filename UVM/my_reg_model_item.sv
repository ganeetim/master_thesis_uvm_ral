`include "uvm_macros.svh"
import uvm_pkg::*;

class my_reg_model_item extends uvm_sequence_item;
  rand my_pkg::target_type target;
  rand logic [31:0] addr;
  rand logic write_enable;
  rand logic [31:0] data;
  logic reset = 1'b1;

  `uvm_object_utils_begin(my_reg_model_item)
    `uvm_field_enum(my_pkg::target_type, target, UVM_ALL_ON)
    `uvm_field_int(addr, UVM_ALL_ON)
    `uvm_field_int(write_enable, UVM_ALL_ON)
    `uvm_field_int(data, UVM_ALL_ON)
    `uvm_field_int(reset, UVM_ALL_ON)
  `uvm_object_utils_end

  function new (string name = "my_reg_model_item");
    super.new(name);
  endfunction

endclass