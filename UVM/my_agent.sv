`include "uvm_macros.svh"
import uvm_pkg::*;

// Agent consists of:
//   Driver
//   Sequencer
//   Monitor
class my_agent extends uvm_agent;
  `uvm_component_utils (my_agent)
	
  my_pkg::my_driver driver_inst;
  my_pkg::my_sequencer sequencer_inst;
  my_pkg::my_monitor monitor_inst;

  function new (string name = "my_agent", uvm_component parent = null);
    super.new(name, parent);
  endfunction

  function void build_phase (uvm_phase phase);
    super.build_phase (phase);
	  
    driver_inst = my_pkg::my_driver::type_id::create("driver_inst", this);
    sequencer_inst = my_pkg::my_sequencer::type_id::create("sequencer_inst", this);
    monitor_inst = my_pkg::my_monitor::type_id::create("monitor_inst", this);
  endfunction

  // Connect Sequencer's and Driver's ports to each other
  function void connect_phase (uvm_phase phase);
    driver_inst.seq_item_port.connect(sequencer_inst.seq_item_export);
  endfunction
endclass