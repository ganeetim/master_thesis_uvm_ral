`include "uvm_macros.svh"
import uvm_pkg::*;

// Driver takes bus transaction from Sequencer and converts it to logic-level transaction
class my_driver extends uvm_driver #(my_pkg::my_reg_model_item);
  `uvm_component_utils (my_driver)
  
  // Bus item to drive
  my_pkg::my_reg_model_item reg_model_item_inst;
  
  // Interface is needed to drive item to DUT
  virtual my_interface interface_inst;

  function new (string name = "my_driver", uvm_component parent = null);
    super.new(name, parent);
  endfunction

  function void build_phase (uvm_phase phase);
    super.build_phase (phase);
    
    // Get interface handle from UVM database
    if(!uvm_config_db#(virtual my_interface)::get(null, "", "interface_inst", interface_inst))
      `uvm_fatal("NOVIF", {"Missing interface_inst: ", get_full_name(), ".interface_inst"})
  endfunction

  task run_phase (uvm_phase phase);
    // Initial assigns
    interface_inst.reg_addr         = 32'hFFFF_FFFF;
    interface_inst.reg_write_enable = 1'b0;
    interface_inst.reg_in           = 32'h0000_0000;
    interface_inst.reg_out          = 32'h0000_0000;
    
    interface_inst.ram_addr         = 32'h0000_0000;
    interface_inst.ram_write_enable = 1'b0;
    interface_inst.ram_in           = 32'h0000_0000;
    interface_inst.ram_out          = 32'h0000_0000;
    
    forever begin
      // Get next item from sequencer
      seq_item_port.get_next_item(reg_model_item_inst);
    
      @(negedge interface_inst.clk);
      
      // Check if this transaction's active-low reset signal is '0'
      if (reg_model_item_inst.reset === 1'b0) begin
        interface_inst.reset = 1'b0;
        
        // Disable reset after the next falling edge of the clock
        @(negedge interface_inst.clk);
        interface_inst.reset = 1'b1;
      end else begin
        // Current transaction is not a reset one, check its target
        // based on a "target" variable in register model sequence item
        case (reg_model_item_inst.target)
          
          my_pkg::REGS : begin
            // Current transaction is executed on registers
        
            // Set the address and write enable signal in interface
            interface_inst.reg_addr = reg_model_item_inst.addr;
            interface_inst.reg_write_enable = reg_model_item_inst.write_enable;
        
            // If it is WRITE transaction - assign data from bus item to interface
            // If it is READ transaction - assign data from interface back to bus item
            if (reg_model_item_inst.write_enable === 1'b1) begin
              interface_inst.reg_in = reg_model_item_inst.data;
            end else begin
              @(posedge interface_inst.clk);
              reg_model_item_inst.data = interface_inst.reg_out;
            end
        
            // Since some of the registers are sensitive to write or read transactions,
            // i.e. for example RC or WC registers,
            // we set register address to a value, that is outside of the range 
            // of the existing registers' addresses
            @(negedge interface_inst.clk);
            interface_inst.reg_addr = 32'hFFFF_FFFF;
          end
          
          my_pkg::RAM : begin
            // Current transaction is executed on RAM
        
            // Set the address and write enable signal in interface
            interface_inst.ram_addr = reg_model_item_inst.addr;
            interface_inst.ram_write_enable = reg_model_item_inst.write_enable;        
        
            // If it is WRITE transaction - assign data from bus item to interface
            // If it is READ transaction - assign data from interface back to bus item
            if (reg_model_item_inst.write_enable === 1'b1) begin
              interface_inst.ram_in = reg_model_item_inst.data;
            end else begin
              @(posedge interface_inst.clk);
              reg_model_item_inst.data = interface_inst.ram_out;
            end
          end
          
          my_pkg::ROM : begin
            // Current transaction is executed on ROM
        
            // Set the address in interface and
            // assign data from interface back to bus item
            interface_inst.rom_addr = reg_model_item_inst.addr;      
            @(posedge interface_inst.clk);
            reg_model_item_inst.data = interface_inst.rom_out;
          end
        endcase
      end
      
      // Wait for a couple of clock cycles
      repeat (3) @(posedge interface_inst.clk);

      // Let the sequence know that this bus item is done and ready to be received
      seq_item_port.item_done();
    end
  endtask
endclass