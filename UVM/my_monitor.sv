`include "uvm_macros.svh"
import uvm_pkg::*;

class my_monitor extends uvm_monitor;
  `uvm_component_utils (my_monitor)
  
  uvm_analysis_port #(my_pkg::my_reg_model_item) monitor_port;
  
  // Bus item to assign values to from interface
  my_pkg::my_reg_model_item reg_model_item_inst;
  
  // Interface is needed to monitor DUT interface
  virtual my_interface interface_inst;

  function new (string name = "my_monitor", uvm_component parent = null);
    super.new(name, parent);
  endfunction

  function void build_phase (uvm_phase phase);
    super.build_phase (phase);
    
    monitor_port = new ("monitor_port", this);
    
    // Get interface handle from UVM database
    if(!uvm_config_db#(virtual my_interface)::get(null, "", "interface_inst", interface_inst))
      `uvm_fatal("NOVIF", {"Missing interface_inst: ", get_full_name(), ".interface_inst"})
  endfunction

  task run_phase (uvm_phase phase);
    forever begin
      reg_model_item_inst = my_pkg::my_reg_model_item::type_id::create("reg_model_item_inst");
      
      // Monitor for any transactions on the interface every rising edge of the clock
      // if current register address is not 32'hFFFF_FFFF
      @(posedge interface_inst.clk iff (interface_inst.reg_addr !== 32'hFFFF_FFFF));
      
      // Discern between write and read transactions
      case (interface_inst.reg_write_enable)
        // Write transaction
        1'b1 : begin
          reg_model_item_inst.target = my_pkg::REGS;
          reg_model_item_inst.addr = interface_inst.reg_addr;
          reg_model_item_inst.write_enable = 1'b1;
          reg_model_item_inst.data = interface_inst.reg_in;
        end
        
        // Read transaction
        1'b0 : begin
          reg_model_item_inst.target = my_pkg::REGS;
          reg_model_item_inst.addr = interface_inst.reg_addr;
          reg_model_item_inst.write_enable = 1'b0;
          reg_model_item_inst.data = interface_inst.reg_out;
        end
      endcase

      // Send caught transaction using analysis port.
      // Do it only in case of single (default) register map.
      `ifndef TWO_MAPS
        monitor_port.write(reg_model_item_inst);
      `endif
    end
  endtask
endclass