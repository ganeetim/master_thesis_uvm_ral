`include "uvm_macros.svh"
import uvm_pkg::*;

// Test that executes built-in UVM RAL sequences
class my_built_in_sequences_test extends my_pkg::my_base_test;
  `uvm_component_utils (my_built_in_sequences_test)
  
  // Reset sequence
  my_pkg::my_reset_seq reset_seq_inst;
  
  // Built-in UVM RAL sequences
  //   Register sequences:
  
  // Checks that the hardware register reset value matches the
  // value specified in the register model
  uvm_reg_hw_reset_seq uvm_reg_hw_reset_seq_inst;
  
  // Executes the uvm_reg_single_bit_bash_seq for each register
  // in the selected block and any sub-blocks it may contain.
  //
  // Sequence uvm_reg_single_bit_bash_seq writes, then check-reads 1's and 0's to
  // all bits of the selected register that have read-write access.
  uvm_reg_bit_bash_seq uvm_reg_bit_bash_seq_inst;
  
  // Executes the uvm_reg_single_access_seq for each
  // register accessible via the selected block.
  //
  // Sequence uvm_reg_single_access_seq writes to the selected register via the frontdoor, checks
  // the value is correctly written via the backdoor, then writes a value via the backdoor
  // and checks that it can be read back correctly via the frontdoor.
  // Repeated for each address map that the register is present in.
  // Requires that the backdoor hdl_path has been specified.
  uvm_reg_access_seq uvm_reg_access_seq_inst;
  
  //   Memory sequences:
  
  // Executes uvm_mem_single_walk_seq on all memories
  // accessible from the specified block.
  //
  // Sequence uvm_mem_single_walk_seq writes a walking pattern into each
  // location in the range of the specified memory, then checks
  // that is read back with the expected value.
  uvm_mem_walk_seq   uvm_mem_walk_seq_inst;
  
  // Executes uvm_mem_single_access_seq for each memory
  // accessible from the specified block.
  //
  // For each location in the range of the specified memory
  // sequence uvm_mem_single_access_seq:
  // Writes via the frontdoor, checks the value written via the
  // backdoor, then writes via the backdoor and reads back via
  // the front door.
  // Repeats test for each address map containing the memory.
  // Requires that the backdoor hdl_path has been specified.
  uvm_mem_access_seq uvm_mem_access_seq_inst;
  
  // Executes the uvm_reg_shared_access_reg_seq on all registers accessible
  // from the specified block.
  // Executes the uvm_mem_shared_access_seq on all memories accessible from
  // the specified block.
  //
  // For each map containing the register, sequence uvm_reg_shared_access_reg_seq
  // writes to the selected register in one map, then check-reads it back
  // from all maps from which it is accessible.
  // Requires that the selected register has been added to
  // multiple address maps.
  //
  // For each map containing the memory, sequence uvm_mem_shared_access_seq 
  // writes to all memory locations and reads back from all using each of
  // the address maps.
  // Requires that the memory has been added to multiple address maps.
  //
  // If there is only one register address map in register block,
  // this sequences does nothing.
  uvm_reg_mem_shared_access_seq uvm_reg_mem_shared_access_seq_inst;
  
  // Used to test that any specified HDL paths defined within a block are
  // accessible by the backdoor access.
  // The check is only performed on registers or memories which have
  // HDL paths declared.
  uvm_reg_mem_hdl_paths_seq uvm_reg_mem_hdl_paths_seq_inst;
  
  function new (string name = "my_built_in_sequences_test", uvm_component parent = null);
    super.new(name, parent);
  endfunction
  
  virtual function void build_phase (uvm_phase phase);
    super.build_phase (phase);

    // Reset sequence
    reset_seq_inst = my_pkg::my_reset_seq::type_id::create("reset_seq_inst");

    // Register sequences
    uvm_reg_hw_reset_seq_inst = uvm_reg_hw_reset_seq::type_id::create("uvm_reg_hw_reset_seq_inst");
    uvm_reg_bit_bash_seq_inst = uvm_reg_bit_bash_seq::type_id::create("uvm_reg_bit_bash_seq_inst");
    uvm_reg_access_seq_inst   = uvm_reg_access_seq::type_id::create("uvm_reg_access_seq_inst");
    
    // Memory sequences
    uvm_mem_walk_seq_inst   = uvm_mem_walk_seq::type_id::create("uvm_mem_walk_seq_inst");
    uvm_mem_access_seq_inst = uvm_mem_access_seq::type_id::create("uvm_mem_access_seq_inst");
    
    uvm_reg_mem_shared_access_seq_inst = uvm_reg_mem_shared_access_seq::type_id::create("uvm_reg_mem_shared_access_seq_inst");
    uvm_reg_mem_hdl_paths_seq_inst     = uvm_reg_mem_hdl_paths_seq::type_id::create("uvm_reg_mem_hdl_paths_seq_inst");
  endfunction
  
  task run_phase (uvm_phase phase);
    phase.raise_objection(this);
  
    // Enables coverage sampling in this register model for
    // specified coverage model(s), sampling is not enabled by default.
    // Returns sum (type uvm_reg_cvr_t with 32 bits) of coverage models with enabled coverage sampling. 
    sum_cov_models = env_inst.ral_env_inst.ral_reg_block_inst.set_coverage(UVM_CVR_ALL);
    
    // Built-in sequences can be disabled using UVM resource database.
    //uvm_resource_db #(bit)::set({"REG::", env_inst.ral_env_inst.ral_reg_block_inst.get_full_name()}, "NO_REG_TESTS", 1);
    
    // Reset sequence needs register model instance (see my_reset_seq.sv for more details)
    reset_seq_inst.ral_reg_block_inst = env_inst.ral_env_inst.ral_reg_block_inst;
    
    // In order to use built-in UVM RAL sequences we need to
    // pass register model instance to them
    uvm_reg_hw_reset_seq_inst.model = env_inst.ral_env_inst.ral_reg_block_inst;
    uvm_reg_bit_bash_seq_inst.model = env_inst.ral_env_inst.ral_reg_block_inst;
    uvm_reg_access_seq_inst.model   = env_inst.ral_env_inst.ral_reg_block_inst;
    
    uvm_mem_walk_seq_inst.model   = env_inst.ral_env_inst.ral_reg_block_inst;
    uvm_mem_access_seq_inst.model = env_inst.ral_env_inst.ral_reg_block_inst;
    
    uvm_reg_mem_shared_access_seq_inst.model = env_inst.ral_env_inst.ral_reg_block_inst;
    uvm_reg_mem_hdl_paths_seq_inst.model     = env_inst.ral_env_inst.ral_reg_block_inst;



    // (Un)comment sequences you want to execute
    
    // In case of reset sequence you need to provide sequencer
    // on which you want to execute the reset sequence
    reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    
    // For a register model sequences argument of start() can be null
    // because sequencer is already set in register model using set_sequencer().
    // Or you can still specify on which sequencer this sequence will be executed:
    // uvm_reg_hw_reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    
    uvm_reg_hw_reset_seq_inst.start(null);
   
    // Before executing any built-in UVM RAL sequence, DUT should be reset:
    reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    uvm_reg_bit_bash_seq_inst.start(null);
    
    reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    uvm_reg_access_seq_inst.start(null);

    reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    uvm_mem_walk_seq_inst.start(null);
    
    reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    uvm_mem_access_seq_inst.start(null);
    
    reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    uvm_reg_mem_shared_access_seq_inst.start(null);
    
    reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    uvm_reg_mem_hdl_paths_seq_inst.start(null);
    
    phase.drop_objection(this);
  endtask
endclass : my_built_in_sequences_test