`include "uvm_macros.svh"
import uvm_pkg::*;

// Base test class that creates the environment for all other sub-tests
class my_base_test extends uvm_test;
  `uvm_component_utils (my_base_test)
  
  my_pkg::my_env env_inst;
  
  // Sum of coverage models with enabled coverage sampling
  uvm_reg_cvr_t sum_cov_models;

  function new (string name = "my_base_test", uvm_component parent = null);
    super.new(name, parent);
  endfunction

  virtual function void build_phase (uvm_phase phase);
    // Here we define which coverage model we should use
    // for a whole register model in this test. We can do that using
    // static method uvm_reg::include_coverage().
    // This method sets up a resource with the key "include_coverage".
    // It is used to control which types of coverage are collected by the register model.
    //
    // Parameters:
    //   string scope               - database resource will be visible in this scope
    //   uvm_reg_cvr_t models       - coverage model to write to database
    //   uvm_object accessor = null - for auditting
    //
    // This method creates a new resource, writes "models" value to it, and sets it into the database using
    // scope {"uvm_reg::", scope} and name "include_coverage" as the lookup parameters.
    // The accessor is used for auditting.
    uvm_reg::include_coverage("*", UVM_CVR_ALL);
    
    super.build_phase (phase);
   
    env_inst = my_pkg::my_env::type_id::create("env_inst", this);
  endfunction
endclass : my_base_test