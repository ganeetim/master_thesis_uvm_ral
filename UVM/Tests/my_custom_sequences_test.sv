`include "uvm_macros.svh"
import uvm_pkg::*;

// Test that executes custom sequence on register model
class my_custom_sequences_test extends my_pkg::my_base_test;
  `uvm_component_utils (my_custom_sequences_test)
  
  // Reset sequence
  my_pkg::my_reset_seq reset_seq_inst;
  
  // Custom sequences
  //   Register sequences:
  my_pkg::my_reg_rw_manual_seq  reg_rw_manual_seq_inst;
  my_pkg::my_reg_rw_mirror_seq  reg_rw_mirror_seq_inst;
  my_pkg::my_reg_ro_manual_seq  reg_ro_manual_seq_inst;
  my_pkg::my_reg_ro_mirror_seq  reg_ro_mirror_seq_inst;
  my_pkg::my_reg_desired_seq    reg_desired_seq_inst;
  my_pkg::my_reg_mirrored_seq   reg_mirrored_seq_inst;
  my_pkg::my_reg_exp_prediction reg_exp_prediction_inst;
  my_pkg::my_all_regs_0_1_seq   all_regs_0_1_seq_inst;
  
  //   Memory sequences:
  my_pkg::my_mem_ram_read_write_seq mem_ram_read_write_seq_inst;
  my_pkg::my_mem_rom_read_write_seq mem_rom_read_write_seq_inst;
  my_pkg::my_mem_burst_seq          mem_burst_seq_inst;
  
  function new (string name = "my_custom_sequences_test", uvm_component parent = null);
    super.new(name, parent);
  endfunction
  
  virtual function void build_phase (uvm_phase phase);
    super.build_phase (phase);

    // Reset sequence
    reset_seq_inst = my_pkg::my_reset_seq::type_id::create("reset_seq_inst");
    
    // Register sequences
    reg_rw_manual_seq_inst  = my_pkg::my_reg_rw_manual_seq::type_id::create("reg_rw_manual_seq_inst");
    reg_rw_mirror_seq_inst  = my_pkg::my_reg_rw_mirror_seq::type_id::create("reg_rw_mirror_seq_inst");
    reg_ro_manual_seq_inst  = my_pkg::my_reg_ro_manual_seq::type_id::create("reg_ro_manual_seq_inst");
    reg_ro_mirror_seq_inst  = my_pkg::my_reg_ro_mirror_seq::type_id::create("reg_ro_mirror_seq_inst");
    reg_desired_seq_inst    = my_pkg::my_reg_desired_seq::type_id::create("reg_desired_seq_inst");
    reg_mirrored_seq_inst   = my_pkg::my_reg_mirrored_seq::type_id::create("reg_mirrored_seq_inst");
    reg_exp_prediction_inst = my_pkg::my_reg_exp_prediction::type_id::create("reg_exp_prediction_inst");
    all_regs_0_1_seq_inst   = my_pkg::my_all_regs_0_1_seq::type_id::create("all_regs_0_1_seq_inst");
    
    // Memory sequences
    mem_ram_read_write_seq_inst = my_pkg::my_mem_ram_read_write_seq::type_id::create("mem_ram_read_write_seq_inst");
    mem_rom_read_write_seq_inst = my_pkg::my_mem_rom_read_write_seq::type_id::create("mem_rom_read_write_seq_inst");
    mem_burst_seq_inst          = my_pkg::my_mem_burst_seq::type_id::create("mem_burst_seq_inst");
    
  endfunction
  
  task run_phase (uvm_phase phase);
    phase.raise_objection(this);
    
    // Enables coverage sampling in this register model for
    // specified coverage model(s), sampling is not enabled by default.
    // Returns sum (type uvm_reg_cvr_t with 32 bits) of coverage models with enabled coverage sampling. 
    sum_cov_models = env_inst.ral_env_inst.ral_reg_block_inst.set_coverage(UVM_CVR_ALL);
    
    // Reset sequence needs register model instance (see my_reset_seq.sv for more details)
    reset_seq_inst.ral_reg_block_inst = env_inst.ral_env_inst.ral_reg_block_inst;
    
    // Set instance of register block to the sequences' register block handle
    reg_rw_manual_seq_inst.ral_reg_block_inst  = env_inst.ral_env_inst.ral_reg_block_inst;
    reg_rw_mirror_seq_inst.ral_reg_block_inst  = env_inst.ral_env_inst.ral_reg_block_inst;
    reg_ro_manual_seq_inst.ral_reg_block_inst  = env_inst.ral_env_inst.ral_reg_block_inst;
    reg_ro_mirror_seq_inst.ral_reg_block_inst  = env_inst.ral_env_inst.ral_reg_block_inst;
    reg_desired_seq_inst.ral_reg_block_inst    = env_inst.ral_env_inst.ral_reg_block_inst;
    reg_mirrored_seq_inst.ral_reg_block_inst   = env_inst.ral_env_inst.ral_reg_block_inst;
    reg_exp_prediction_inst.ral_reg_block_inst = env_inst.ral_env_inst.ral_reg_block_inst;
    all_regs_0_1_seq_inst.ral_reg_block_inst   = env_inst.ral_env_inst.ral_reg_block_inst;
    
    mem_ram_read_write_seq_inst.ral_reg_block_inst = env_inst.ral_env_inst.ral_reg_block_inst;
    mem_rom_read_write_seq_inst.ral_reg_block_inst = env_inst.ral_env_inst.ral_reg_block_inst;
    mem_burst_seq_inst.ral_reg_block_inst          = env_inst.ral_env_inst.ral_reg_block_inst;
    
    
    
    // (Un)comment sequences you want to execute
    
    // In case of reset sequence you need to provide sequencer
    // on which you want to execute the reset sequence
    reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    
    // For a register model sequences argument of start() can be null
    // because sequencer is already set in register model using set_sequencer().
    // Or you can still specify on which sequencer this sequence will be executed:
    // reg_rw_manual_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    
    reg_rw_manual_seq_inst.start(null);
    reg_rw_mirror_seq_inst.start(null);
    reg_ro_manual_seq_inst.start(null);
    reg_ro_mirror_seq_inst.start(null);
    reg_desired_seq_inst.start(null);
    reg_mirrored_seq_inst.start(null);
    reg_exp_prediction_inst.start(env_inst.agent_inst.sequencer_inst);
    
    // Invoke reset after previous sequences, which changed mirrored values
    // in some registers:
    reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    
    all_regs_0_1_seq_inst.start(null);
    
    mem_ram_read_write_seq_inst.start(null);
    mem_rom_read_write_seq_inst.start(null);
    mem_burst_seq_inst.start(null);
    
    phase.drop_objection(this);
  endtask
endclass : my_custom_sequences_test