`include "uvm_macros.svh"
import uvm_pkg::*;

// This test executed custom and built-in UVM RAL sequences.
// Expected coverage after executing is 100%.
class my_full_test extends my_pkg::my_base_test;
  `uvm_component_utils (my_full_test)
  
  // Reset sequence
  my_pkg::my_reset_seq reset_seq_inst;
  
  // Custom sequences
  //   Register sequences:
  my_pkg::my_reg_rw_manual_seq reg_rw_manual_seq_inst;
  my_pkg::my_reg_rw_mirror_seq reg_rw_mirror_seq_inst;
  my_pkg::my_reg_ro_manual_seq reg_ro_manual_seq_inst;
  my_pkg::my_reg_ro_mirror_seq reg_ro_mirror_seq_inst;
  my_pkg::my_all_regs_0_1_seq  all_regs_0_1_seq_inst;
  
  //   Memory sequences:
  my_pkg::my_mem_ram_read_write_seq mem_ram_read_write_seq_inst;
  my_pkg::my_mem_rom_read_write_seq mem_rom_read_write_seq_inst;
  my_pkg::my_mem_burst_seq          mem_burst_seq_inst; 
  
  // Built-in UVM RAL sequences
  //   Register sequences:
  uvm_reg_hw_reset_seq uvm_reg_hw_reset_seq_inst;
  uvm_reg_bit_bash_seq uvm_reg_bit_bash_seq_inst;
  uvm_reg_access_seq   uvm_reg_access_seq_inst;
  
  //   Memory sequences:
  uvm_mem_walk_seq   uvm_mem_walk_seq_inst;
  uvm_mem_access_seq uvm_mem_access_seq_inst;
  
  uvm_reg_mem_shared_access_seq uvm_reg_mem_shared_access_seq_inst;
  uvm_reg_mem_hdl_paths_seq     uvm_reg_mem_hdl_paths_seq_inst;

  function new (string name = "my_full_test", uvm_component parent = null);
    super.new(name, parent);
  endfunction
  
  virtual function void build_phase (uvm_phase phase);
    super.build_phase (phase);

    // Reset sequence
    reset_seq_inst = my_pkg::my_reset_seq::type_id::create("reset_seq_inst");
    
    // Custom sequences
    //   Register sequences:
    reg_rw_manual_seq_inst = my_pkg::my_reg_rw_manual_seq::type_id::create("reg_rw_manual_seq_inst");
    reg_rw_mirror_seq_inst = my_pkg::my_reg_rw_mirror_seq::type_id::create("reg_rw_mirror_seq_inst");
    reg_ro_manual_seq_inst = my_pkg::my_reg_ro_manual_seq::type_id::create("reg_ro_manual_seq_inst");
    reg_ro_mirror_seq_inst = my_pkg::my_reg_ro_mirror_seq::type_id::create("reg_ro_mirror_seq_inst");
    all_regs_0_1_seq_inst  = my_pkg::my_all_regs_0_1_seq::type_id::create("all_regs_0_1_seq_inst");
    
    //   Memory sequences:
    mem_ram_read_write_seq_inst = my_pkg::my_mem_ram_read_write_seq::type_id::create("mem_ram_read_write_seq_inst");
    mem_rom_read_write_seq_inst = my_pkg::my_mem_rom_read_write_seq::type_id::create("mem_rom_read_write_seq_inst");
    mem_burst_seq_inst          = my_pkg::my_mem_burst_seq::type_id::create("mem_burst_seq_inst");

    // Built-in UVM RAL sequences
    //   Register sequences:
    uvm_reg_hw_reset_seq_inst = uvm_reg_hw_reset_seq::type_id::create("uvm_reg_hw_reset_seq_inst");
    uvm_reg_bit_bash_seq_inst = uvm_reg_bit_bash_seq::type_id::create("uvm_reg_bit_bash_seq_inst");
    uvm_reg_access_seq_inst   = uvm_reg_access_seq::type_id::create("uvm_reg_access_seq_inst");
    
    //   Memory sequences:
    uvm_mem_walk_seq_inst   = uvm_mem_walk_seq::type_id::create("uvm_mem_walk_seq_inst");
    uvm_mem_access_seq_inst = uvm_mem_access_seq::type_id::create("uvm_mem_access_seq_inst");
    
    uvm_reg_mem_shared_access_seq_inst = uvm_reg_mem_shared_access_seq::type_id::create("uvm_reg_mem_shared_access_seq_inst");
    uvm_reg_mem_hdl_paths_seq_inst     = uvm_reg_mem_hdl_paths_seq::type_id::create("uvm_reg_mem_hdl_paths_seq_inst");
  endfunction
  
  task run_phase (uvm_phase phase);
    phase.raise_objection(this);
    
    // Enables coverage sampling in this register model for
    // specified coverage model(s), sampling is not enabled by default.
    // Returns sum (type uvm_reg_cvr_t with 32 bits) of coverage models with enabled coverage sampling. 
    sum_cov_models = env_inst.ral_env_inst.ral_reg_block_inst.set_coverage(UVM_CVR_ALL);
    
    // Built-in sequences can be disabled using UVM resource database.
    //uvm_resource_db #(bit)::set({"REG::", env_inst.ral_env_inst.ral_reg_block_inst.get_full_name()}, "NO_REG_TESTS", 1);
    
    // Reset sequence needs register model instance (see my_reset_seq.sv for more details)
    reset_seq_inst.ral_reg_block_inst = env_inst.ral_env_inst.ral_reg_block_inst;
    
    // Set instance of register block to the sequences' register block handle
    reg_rw_manual_seq_inst.ral_reg_block_inst = env_inst.ral_env_inst.ral_reg_block_inst;
    reg_rw_mirror_seq_inst.ral_reg_block_inst = env_inst.ral_env_inst.ral_reg_block_inst;
    reg_ro_manual_seq_inst.ral_reg_block_inst = env_inst.ral_env_inst.ral_reg_block_inst;
    reg_ro_mirror_seq_inst.ral_reg_block_inst = env_inst.ral_env_inst.ral_reg_block_inst;
    all_regs_0_1_seq_inst.ral_reg_block_inst  = env_inst.ral_env_inst.ral_reg_block_inst;
    
    mem_ram_read_write_seq_inst.ral_reg_block_inst = env_inst.ral_env_inst.ral_reg_block_inst;
    mem_rom_read_write_seq_inst.ral_reg_block_inst = env_inst.ral_env_inst.ral_reg_block_inst;
    mem_burst_seq_inst.ral_reg_block_inst          = env_inst.ral_env_inst.ral_reg_block_inst;
    
    // In order to use built-in UVM RAL sequences we need to
    // pass register model instance to them
    uvm_reg_hw_reset_seq_inst.model = env_inst.ral_env_inst.ral_reg_block_inst;
    uvm_reg_bit_bash_seq_inst.model = env_inst.ral_env_inst.ral_reg_block_inst;
    uvm_reg_access_seq_inst.model   = env_inst.ral_env_inst.ral_reg_block_inst;
    
    uvm_mem_walk_seq_inst.model   = env_inst.ral_env_inst.ral_reg_block_inst;
    uvm_mem_access_seq_inst.model = env_inst.ral_env_inst.ral_reg_block_inst;
    
    uvm_reg_mem_shared_access_seq_inst.model = env_inst.ral_env_inst.ral_reg_block_inst;
    uvm_reg_mem_hdl_paths_seq_inst.model     = env_inst.ral_env_inst.ral_reg_block_inst;
    
    
    
    // (Un)comment sequences you want to execute
    // Note for Questa users: By default, the Transcript window retains the last 5000 lines of output from the transcript.
    //  You can change this default by clicking to Transcript window, then selecting Transcript > Saved Lines.
    //  Setting this variable to 0 instructs the tool to retain all lines of the transcript.
    
    // In case of reset sequence you need to provide sequencer
    // on which you want to execute the reset sequence
    reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);

    // For a register model sequences argument of start() can be null
    // because sequencer is already set in register model using set_sequencer().
    // Or you can still specify on which sequencer this sequence will be executed:
    // reg_rw_manual_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    
    reg_rw_manual_seq_inst.start(null);
    reg_rw_mirror_seq_inst.start(null);
    reg_ro_manual_seq_inst.start(null);
    reg_ro_mirror_seq_inst.start(null);
    all_regs_0_1_seq_inst.start(null);
    
    mem_ram_read_write_seq_inst.start(null);
    mem_rom_read_write_seq_inst.start(null);
    mem_burst_seq_inst.start(null);
    
    // Before executing any built-in UVM RAL sequence, DUT should be reset:
    reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    uvm_reg_hw_reset_seq_inst.start(null);
    
    reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    uvm_reg_bit_bash_seq_inst.start(null);
    
    reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    uvm_reg_access_seq_inst.start(null);
    
    reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    uvm_mem_walk_seq_inst.start(null);
    
    reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    uvm_mem_access_seq_inst.start(null);
    
    reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    uvm_reg_mem_shared_access_seq_inst.start(null);
    
    reset_seq_inst.start(env_inst.agent_inst.sequencer_inst);
    uvm_reg_mem_hdl_paths_seq_inst.start(null);
    
    phase.drop_objection(this);
  endtask
endclass : my_full_test