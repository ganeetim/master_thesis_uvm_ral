interface my_interface;
  logic clk;
  logic reset;
  
  // Signals that communicate with registers
  logic [31:0] reg_addr;
  logic reg_write_enable;
  logic [31:0] reg_in;
  logic [31:0] reg_out;
  
  // Signals that communicate with RAM
  logic [31:0] ram_addr;
  logic ram_write_enable;
  logic [31:0] ram_in;
  logic [31:0] ram_out;
  
  // Signals that communicate with ROM
  logic [31:0] rom_addr;
  logic [31:0] rom_out;
endinterface